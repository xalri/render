#version 450 core

uniform sampler2DArray surface_light;
uniform sampler2DArray volume_light;
uniform sampler2DArray in_depth;

in vec2 uv;

layout(location=0) out vec4 out_col;

vec3 ACESFilm(in vec3 x) {
    float a = 2.51f;
    float b = 0.03f;
    float c = 2.43f;
    float d = 0.59f;
    float e = 0.14f;
    return clamp((x*(a*x+b))/(x*(c*x+d)+e), vec3(0.0), vec3(1.0));
}

float z(vec3 uv, float bias) {
    const float near = 0.01;
    const float far = 100.0;
    float z_b = texture(in_depth, uv, bias).r;
    float z_n = 2.0 * z_b - 1.0;
    return 2.0 * near / (far + near - z_n * (far - near));
}

float focus(float depth, float avr_depth) {
    float focal_depth_near = avr_depth * 0.6;
    float focal_depth_far = avr_depth * 1.2;
    float depth_diff = 1.0;
    if (depth < focal_depth_near)
        depth_diff = (pow(mix(0.0, 1.0, depth / focal_depth_near), mix(2.0, 1.0, focal_depth_near)));
    if(depth > focal_depth_far)
        depth_diff = pow(mix(1.0, 0.0, (depth - focal_depth_far) / (1.0 - focal_depth_far)), mix(7.0, 1.0, focal_depth_far));

    depth_diff = clamp(depth_diff, 0.0, 1.0);
    return depth_diff;
}

void main() {
    vec3 _uv = vec3(uv, 0.0);
    vec2 pixel_size = vec2(1.0) / textureSize(surface_light, 0).xy;
    vec3 col = texture(surface_light, _uv).xyz;
    float depth = z(_uv, 0.0);

    float avr_brightness = length(textureLod(surface_light, vec3(0.5, 0.5, 0.0), 20.0));
    float avr_depth = z(vec3(0.5, 0.5, 0.0), 6.0);
    avr_depth = clamp(avr_depth, 0.001, 0.7);

    // Convolutional effects (bloom, fake SSAO, DOF):

    float kernel15[15][15] = {
        { 0.000082, 0.000167, 0.000306, 0.000502, 0.000738, 0.000971, 0.001146, 0.001211, 0.001146, 0.000971, 0.000738, 0.000502, 0.000306, 0.000167, 0.000082},
        { 0.000167, 0.000341, 0.000625, 0.001026, 0.001509, 0.001987, 0.002344, 0.002476, 0.002344, 0.001987, 0.001509, 0.001026, 0.000625, 0.000341, 0.000167},
        { 0.000306, 0.000625, 0.001146, 0.001881, 0.002765, 0.003641, 0.004294, 0.004537, 0.004294, 0.003641, 0.002765, 0.001881, 0.001146, 0.000625, 0.000306},
        { 0.000502, 0.001026, 0.001881, 0.003086, 0.004537, 0.005975, 0.007047, 0.007446, 0.007047, 0.005975, 0.004537, 0.003086, 0.001881, 0.001026, 0.000502},
        { 0.000738, 0.001509, 0.002765, 0.004537, 0.00667, 0.008783, 0.01036, 0.010946, 0.01036, 0.008783, 0.00667, 0.004537, 0.002765, 0.001509, 0.000738},
        { 0.000971, 0.001987, 0.003641, 0.005975, 0.008783, 0.011566, 0.013643, 0.014414, 0.013643, 0.011566, 0.008783, 0.005975, 0.003641, 0.001987, 0.000971},
        { 0.001146, 0.002344, 0.004294, 0.007047, 0.01036, 0.013643, 0.016092, 0.017002, 0.016092, 0.013643, 0.01036, 0.007047, 0.004294, 0.002344, 0.001146},
        { 0.001211, 0.002476, 0.004537, 0.007446, 0.010946, 0.014414, 0.017002, 0.017965, 0.017002, 0.014414, 0.010946, 0.007446, 0.004537, 0.002476, 0.001211},
        { 0.001146, 0.002344, 0.004294, 0.007047, 0.01036, 0.013643, 0.016092, 0.017002, 0.016092, 0.013643, 0.01036, 0.007047, 0.004294, 0.002344, 0.001146},
        { 0.000971, 0.001987, 0.003641, 0.005975, 0.008783, 0.011566, 0.013643, 0.014414, 0.013643, 0.011566, 0.008783, 0.005975, 0.003641, 0.001987, 0.000971},
        { 0.000738, 0.001509, 0.002765, 0.004537, 0.00667, 0.008783, 0.01036, 0.010946, 0.01036, 0.008783, 0.00667, 0.004537, 0.002765, 0.001509, 0.000738},
        { 0.000502, 0.001026, 0.001881, 0.003086, 0.004537, 0.005975, 0.007047, 0.007446, 0.007047, 0.005975, 0.004537, 0.003086, 0.001881, 0.001026, 0.000502},
        { 0.000306, 0.000625, 0.001146, 0.001881, 0.002765, 0.003641, 0.004294, 0.004537, 0.004294, 0.003641, 0.002765, 0.001881, 0.001146, 0.000625, 0.000306},
        { 0.000167, 0.000341, 0.000625, 0.001026, 0.001509, 0.001987, 0.002344, 0.002476, 0.002344, 0.001987, 0.001509, 0.001026, 0.000625, 0.000341, 0.000167},
        { 0.000082, 0.000167, 0.000306, 0.000502, 0.000738, 0.000971, 0.001146, 0.001211, 0.001146, 0.000971, 0.000738, 0.000502, 0.000306, 0.000167, 0.000082},
    };
    int kernel_w = 7;

    float dof_threshold = mix(0.001, 0.017, focus(depth, avr_depth));
    vec3 bloom_accum = vec3(0.0);
    vec3 dof_accum = vec3(0.0);
    float dof_n = 0;

    for(int x = -kernel_w; x <= kernel_w; x++) {
        for(int y = -kernel_w; y <= kernel_w; y++) {
            float weight = kernel15[x + kernel_w][y + kernel_w];
            vec2 offset = vec2(x, y) * pixel_size;
            vec3 sample_pos = vec3(uv + offset, 0.0);
            vec3 _col = texture(surface_light, sample_pos).xyz;

            bloom_accum += float(length(_col) > 2.5) * weight * _col;

            if (weight > dof_threshold) {
                dof_n += 1;
                dof_accum += _col;
            }
        }
    }
    if (dof_n > 0) col = dof_accum / dof_n;
    //col = max(col, bloom_accum);

    // Vignetting
    float len = length(uv - 0.5);
    float vignette = smoothstep(0.8, 0.3, len);

    col += texture(volume_light, _uv).xyz; // Volumetric light
    //col /= avr_brightness * 1.1; // HDR
    col = mix(col, col * vignette, 0.6); // Vignette
    col = ACESFilm(col);
    col = pow(col, vec3(1.11));
    col = clamp(col, vec3(0), vec3(1));

    out_col = vec4(col, 1.0);
}
