#version 450 core

layout(location = 0) uniform mat4 view;
layout(location = 1) uniform mat4 projection;
out vec3 local_pos;

void main() {
    int r = int(gl_VertexID > 6);
    int i = r == 1 ? 13 - gl_VertexID : gl_VertexID;
    int x = int(i < 3 || i == 4);
    int y = r ^ int(i > 0 && i < 4);
    int z = r ^ int(i < 2 || i > 5);

    local_pos = (vec3(x, y, z) * 2.0f) - 1.0f;
    gl_Position = (projection * mat4(mat3(view)) * vec4(local_pos, 1.0f)).xyww;
}
