#version 450 core

struct Attrs {
    vec4 col_factor;
    vec3 emissive_factor;
    float metallic_factor;
    float roughness_factor;
    int col_tx;
    int mr_tx;
    int ao_tx;
    int normal_tx;
    int emissive_tx;
    uint _padding[2];
};

in Vertex {
    flat uint attrs_idx;
    vec3 normal;
    vec2 uv;
    vec3 world_pos;
} IN;

uniform sampler2DArray tx;
uniform vec3 eye_position;

layout(std430, binding = 2) buffer attr_buf { Attrs attrs[]; };
layout(std430, binding = 3) buffer tx_size_buf { vec2 tx_size[]; };

layout(location=0) out vec4 out_col;
layout(location=1) out vec3 out_emission;
layout(location=2) out vec3 out_norm;
layout(location=3) out vec3 out_mro;

// FROM http://www.thetenthplanet.de/archives/1180
mat3 cotangent_frame( vec3 N, vec3 p, vec2 uv ) {
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );

    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // construct a scale-invariant frame
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}

vec4 get_texel(in int layer, in vec2 uv) {
    if (layer < 0) {
        return vec4(1.0);
    } else {
        return texture(tx, vec3(uv * tx_size[layer], layer));
    }
}

void main() {
    Attrs at = attrs[IN.attrs_idx];
    vec4 tx_col = get_texel(at.col_tx, IN.uv);
    if (tx_col.a < 0.01) discard;
    // Diffuse textures are sRGB, convert to linear
    tx_col = vec4(pow(vec3(tx_col), vec3(2.2)), tx_col.a);
    vec4 col = at.col_factor * tx_col;
    float ao = get_texel(at.ao_tx, IN.uv).r;
    vec3 emission = vec3(0.0);
    if (at.emissive_tx >= 0) emission = get_texel(at.emissive_tx, IN.uv).rgb * at.emissive_factor;

    float metallic = at.metallic_factor;
    float roughness = at.roughness_factor;
    if (at.mr_tx >= 0) {
        vec4 mr = get_texel(at.mr_tx, IN.uv);
        metallic *= mr.b;
        roughness *= mr.g;
    }

    vec3 v = normalize(eye_position - IN.world_pos); // relative position of the camera
    vec3 n = normalize(IN.normal); // fragment's world space normal

    if (at.normal_tx >= 0) {
        vec3 tx_normal = 2.0f * get_texel(at.normal_tx, IN.uv).rgb - 1.0f;
        tx_normal.y *= 0.5;
        mat3 TBN = cotangent_frame(n, -v, IN.uv);
        n = normalize( TBN * tx_normal );
    }

    out_col = col;
    out_emission = emission;
    out_norm = n;
    out_mro = vec3(metallic, roughness, ao);
}

