#version 450
#extension GL_ARB_shader_draw_parameters : enable

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

struct DustParticle {
    vec3 position;
    bool alive;
    vec3 velocity;
    int _pad1;
    vec3 orientation;
    int _pad2;
};

layout(std430, binding = 0) buffer dust_buf { DustParticle dust[]; };
uniform int init;
uniform float dt;

#pragma include glsl/random.glsl

void main() {
    const vec3 range = vec3(12.0);
    const float vel = 0.0003;

    DustParticle p = dust[gl_GlobalInvocationID.x];
    if (bool(init) || !p.alive) {
        p.position = rand_vec3(1) * range;
        p.velocity = normalize(rand_vec3(2)) * vel;
        p.orientation = normalize(rand_vec3(3));
        p.alive = true;
    }
    p.position += p.velocity * dt;

    if (abs(p.position.x) > range.x || abs(p.position.y) > range.y || abs(p.position.z) > range.z ) p.alive = false;

    p.orientation = normalize(p.orientation + p.velocity * 0.000001);
    dust[gl_GlobalInvocationID.x] = p;
}
