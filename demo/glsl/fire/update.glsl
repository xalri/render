#version 450
#extension GL_ARB_shader_draw_parameters : enable

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

struct FireParticle {
    vec3 position;
    bool alive;
    vec3 velocity;
    float life;
    float life_drain;
    uint kind;
};

layout(std430, binding = 0) buffer fire_buf { FireParticle fire[]; };

uniform int init;
uniform float dt = 1.0;

#pragma include glsl/random.glsl

const float max_lifetime = 140.0;

FireParticle new_particle(float s) {
    vec3 relative_pos = pow(rand_vec3(s - 1.12), vec3(2.0));
    relative_pos.y = 0.0;

    vec3 pos = relative_pos * 0.07;
    vec3 vel = (rand_vec3(s + 2.432) + vec3(0.0, 6.0, 0.0)) * 0.0007;
    float life = (max_lifetime / 2.0) + random(s) * max_lifetime / 2.0;
    uint kind = gl_GlobalInvocationID.x % 8; // TODO: uniform

    return FireParticle(pos, true, vel, 1.0, 1.0 / life, kind);
}

FireParticle update(FireParticle p, float s) {
    vec3 towards_center = -vec3(p.position.x, 0.0, p.position.z);
    vec3 normal = normalize(cross(towards_center, p.velocity));

    p.velocity += dt * towards_center * 0.0008;
    p.velocity += dt * normal * 0.000004;

    p.position += p.velocity * dt;
    p.life -= p.life_drain * dt;
    if (p.life <= 0.0) p.alive = false;
    return p;
}

void main() {
    FireParticle p = fire[gl_GlobalInvocationID.x];
    float s = float(gl_GlobalInvocationID.x) * 5.4285 + seed;

    if (bool(init) || !p.alive) p = new_particle(s);

    if (bool(init)) {
        float max = random(s) * (p.life / p.life_drain) / dt;
        for (int i = 0; i < max; i++) {
            p = update(p, s);
            s = s * 0.648 + 1.43;
        }
    }

    FireParticle tmp = update(p, s);
    p = update(p, s);

    fire[gl_GlobalInvocationID.x] = p;
}
