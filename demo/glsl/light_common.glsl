// Properties of a fragment being shaded
struct Fragment {
    vec3 view_pos;
    vec3 world_pos;
    vec3 emission;
    vec3 normal;
    vec3 albedo;
    float roughness;
    float metallic;
    float occlusion;
};

struct SpotLight {
    vec3 position;
    float range;
    vec3 colour;
    float inner_cone;
    vec3 forward;
    float outer_cone;
    bool has_shadow;
    int shadow_map_idx;
    mat4 shadow_pv;
};

struct TubeLight {
    vec3 position_a;
    float range;
    vec3 position_b;
    float radius;
    vec3 colour;
};

struct SunLight {
    vec3 direction;
    bool has_shadow;
    vec3 colour;
    int shadow_map_idx;
    mat4 shadow_pv;
};

struct LightProbe {
    int map_idx;
    float factor;
};

// A punctual light with pre-calculated direction & visibility.
struct Light {
    vec3 color;
    // Direction to the light. For a line or spot light, direction is given by
    // the normalised difference between light position & fragment position.
    vec3 direction;
    // visibility derived from shadow maps, light cone, and attenuation.
    float visibility;
};

uniform sampler2DArray in_col;
uniform sampler2DArray in_depth;
uniform sampler2DArray in_emission;
uniform sampler2DArray in_norm;
uniform sampler2DArray in_mro;

uniform mat4 inv_perspective;
uniform mat4 inv_view;
uniform vec3 eye_position;
uniform samplerCubeArray cube_maps;
uniform sampler2DArray shadow_maps;
uniform uint n_spot_lights;
uniform uint n_tube_lights;
uniform uint n_sun_lights;
uniform uint n_probes;

layout(std430, binding = 5) readonly buffer spot_light_buf { SpotLight spot_lights[]; };
layout(std430, binding = 6) readonly buffer tube_light_buf { TubeLight tube_lights[]; };
layout(std430, binding = 7) readonly buffer sun_light_buf  { SunLight sun_lights[]; };
layout(std430, binding = 8) readonly buffer probe_buf      { LightProbe probes[]; };

in vec2 uv;

layout(location=0) out vec4 out_col;

Fragment current_frag() {
    vec3 _uv = vec3(uv, 0.0);

    // Calculate the position of the fragment in view space & world space:
    float depth = texture(in_depth, _uv).r;
    vec3 clip_pos = vec3(uv, depth) * 2.0 - 1.0;
    vec4 view_pos = inv_perspective * vec4(clip_pos, 1.0);
    view_pos /= view_pos.w;
    vec3 world_pos = (inv_view * view_pos).xyz; // world-space position of the fragment

    vec3 mro = texture(in_mro, _uv).xyz;
    return Fragment(
        /* view_pos  */ view_pos.xyz,
        /* world_pos */ world_pos,
        /* emission  */ texture(in_emission, _uv).xyz,
        /* normal    */ texture(in_norm, _uv).xyz,
        /* albedo    */ texture(in_col, _uv).xyz,
        /* roughness */ mro.y,
        /* metallic  */ mro.x,
        /* occlusion */ mro.z);
}

float saturate(in float f) {
    return clamp(f, 0.0, 1.0);
}

#define PI 3.1415926535

// Roughness & metallicity PBR lighting
// https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
vec3 eval_light(Fragment frag, Light light, vec3 view_direction) {
    if (light.visibility == 0) return vec3(0.0);

    vec3 radiance = light.color * light.visibility * frag.occlusion;

    vec3 n = frag.normal;
    vec3 v = view_direction;
    vec3 l = light.direction;
    vec3 h = normalize(v + l);

    float nv = saturate(dot(n, v));
    float nl = saturate(dot(n, l));
    float nh = saturate(dot(n, h));
    float vh = saturate(dot(v, h));

    // microgeometry NDF:
    float a = frag.roughness * frag.roughness;
    float a2 = a * a;
    float denom = (nh * nh * (a2 - 1.0f) + 1.0f);
    float d = a2 / (PI * denom * denom);

    // geometry function:
    float k = (frag.roughness + 1) * (frag.roughness + 1) / 8.0f;
    float ggx1 = nl / (nl * (1 - k) + k);
    float ggx2 = nv / (nv * (1 - k) + k);
    float g = ggx1 * ggx2;

    // Schlick approximation of fresnel:
    vec3 f0 = vec3(0.04);
    f0 = mix(f0, frag.albedo, frag.metallic);
    vec3 f = f0 + (1 - f0) * pow(2, (-5.55473 * nh - 6.98316) * nh);

    // Combined specular:
    vec3 specular = (f * g * d) / max(0.001, 4 * nl * nv);

    // Diffuse factor
    vec3 kd = (vec3(1.0) - f) * (1.0 - frag.metallic);

    return radiance * (frag.albedo * kd / PI + specular) * nl;
}

float shadow(vec3 world_pos, int idx, mat4 pv, bool pcf) {
    // TODO: soft shadows (pcf kernel based on distance to occluder)
    float visibility = 1.0;
    vec2 shadow_map_size = textureSize(shadow_maps, 0).xy;
    uint max_shadows = uint(textureSize(shadow_maps, 0).z);
    vec4 shadow_pos = pv * vec4(world_pos, 1.0);
    shadow_pos = ((shadow_pos / shadow_pos.w) + 1.0) / 2.0f;

    const float bias = 0.002;
    const float kernel_size = 2.5;
    const float diff_per_sample = 0.99 / (6 * kernel_size * kernel_size);

    if (pcf) {
        for (float x = -kernel_size; x <= kernel_size; x++) {
            for (float y = -kernel_size; y <= kernel_size; y++) {
                vec2 offset = vec2(x, y) / shadow_map_size;
                if (texture(shadow_maps, vec3(shadow_pos.xy + offset, idx)).r < shadow_pos.z - bias) {
                    visibility -= diff_per_sample;
                }
            }
        }
    } else {
        visibility = float(texture(shadow_maps, vec3(shadow_pos.xy, idx)).r > shadow_pos.z - bias);
    }

    return visibility;
}

vec3 closest_point_on_line(vec3 a, vec3 b, vec3 p) {
    vec3 c = p - a;
    vec3 v = normalize(b - a);
    float d = length(b - a);
    float t = dot(v, c);
    if (t < 0) return a;
    if (t > d) return b;
    return a + v * t;
}

float attenuate(vec3 diff) {
    float dist2 = length(diff) * length(diff);
    return 1.0 / dist2; // TODO: faster attenuation falloff at end of light range.
}

float spot_light_visibility(vec3 world_pos, SpotLight light, bool pcf) {
    vec3 diff = world_pos - light.position;
    float visibility = attenuate(diff);

    float theta = max(0.0, dot(normalize(diff), normalize(-light.forward)));
    float epsilon = light.inner_cone - light.outer_cone;
    visibility *= saturate(theta - light.outer_cone);

    if (light.has_shadow) visibility *= shadow(world_pos, light.shadow_map_idx, light.shadow_pv, pcf);

    return saturate(visibility * 100.0);
}

float sun_light_visibility(vec3 world_pos, SunLight light, bool pcf) {
    float visibility = 1.0;
    if (light.has_shadow) visibility = shadow(world_pos, light.shadow_map_idx, light.shadow_pv, pcf);

    return visibility;
}

// Non-PBR image based lighting, estimates roughness & diffuse with mipmaps.
// http://casual-effects.blogspot.com/2011/08/plausible-environment-lighting-in-two.html
vec3 eval_ibl(Fragment frag, LightProbe ibl, vec3 view_direction) {
    float env_w = ibl.map_idx;
    vec3 n = frag.normal;
    vec3 v = view_direction;

    float nv = saturate(dot(n, v));
    vec3 f0 = vec3(0.04);
    f0 = mix(f0, frag.albedo, frag.metallic);
    vec3 f = f0 + (max(vec3(1.0 - frag.roughness), f0) - f0) * pow(2, (-5.55473 * nv - 6.98316) * nv);
    vec3 ks = f;
    vec3 kd = (vec3(1.0) - ks) * (1.0 - frag.metallic);

    vec3 irradiance = textureLod(cube_maps, vec4(n, env_w), 9).rgb;
    vec3 diffuse = irradiance * vec3(frag.albedo);
    float spec_lod = pow(frag.roughness, 0.5) * 10.0;
    vec3 specular = textureLod(cube_maps, vec4(reflect(-v, n), env_w), spec_lod).rgb;

    return ibl.factor * (kd * diffuse + ks * specular);
}
