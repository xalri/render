uniform float seed;

uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

float random( float f ) {
    const uint mantissaMask = 0x007FFFFFu;
    const uint one          = 0x3F800000u;
    uint h = hash( floatBitsToUint( f ) );
    h &= mantissaMask;
    h |= one;
    float  r2 = uintBitsToFloat( h );
    return r2 - 1.0;
}

vec3 rand_vec3(float s) {
    s += seed * 157.0 + float(gl_GlobalInvocationID.x) * 19247.2;
    return vec3(random(s), random(s * 0.8), random(s * 1.4)) * 2.0 - 1.0;
}

