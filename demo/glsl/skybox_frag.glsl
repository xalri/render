#version 450 core

uniform samplerCubeArray cube_maps;
uniform int env_map_idx;

in vec3 local_pos;
out vec3 out_emission;

void main() {
    out_emission = texture(cube_maps, vec4(normalize(local_pos), env_map_idx)).xyz;
}
