#version 450 core

#pragma include glsl/light_common.glsl

void main() {
    // Unpack G-buffer
    Fragment frag = current_frag();

    // fragment -> camera vector
    vec3 v = normalize(eye_position - frag.world_pos);
    vec3 r = reflect(-v, frag.normal);

    // Accumulate light:
    vec3 light_accum = vec3(0.1) * frag.albedo;
    light_accum += frag.emission;

    // Non-PBR IBL:
    for (int i = 0; i < n_probes; i++) {
        LightProbe l = probes[i];
        light_accum += eval_ibl(frag, l, v);
    }

    // Tube lights using representitive points:
    for (int i = 0; i < n_tube_lights; i++) {
        TubeLight l = tube_lights[i];
        vec3 l1 = l.position_a;
        vec3 l2 = l.position_b;

        // Attenuation is based on the closest point to the tube, then we estimate light
        // from a representitive point based on the reflection vector.
        // TODO: conservation of energy?:
        // https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
        vec3 close = closest_point_on_line(l1, l2, frag.world_pos) - frag.world_pos;
        vec3 rp = dot(close, r) * r + close;
        close = close - rp * saturate(l.radius / length(rp));

        float vis = attenuate(close);
        light_accum += eval_light(frag, Light(l.colour, normalize(rp), vis), v);
    }

    // Shadowed spot lights
    for (int i = 0; i < n_spot_lights; i++) {
        SpotLight l = spot_lights[i];
        light_accum += eval_light(frag, Light(l.colour, l.forward,
            spot_light_visibility(frag.world_pos, l, true)), v);
    }

    // Shadowed directional lights
    for (int i = 0; i < n_sun_lights; i++) {
        SunLight l = sun_lights[i];
        light_accum += eval_light(frag, Light( l.colour, l.direction,
            sun_light_visibility(frag.world_pos, l, true)), v);
    }

    out_col = vec4(light_accum, 1.0);
}