
#include <vector>
#include <sstream>
#include <iomanip>
#include <stb_image.h>
#include "nuklear.hpp"
#include "xal_time.h"
#include "window.hpp"
#include "camera.hpp"
#include "common.hpp"
#include "context.hpp"
#include "alloc.hpp"
#include "draw.hpp"
#include "cubemap.hpp"
#include "gltf.hpp"
#include "light.hpp"
#include "colour.hpp"
#include "text.hpp"

struct DustParticle {
    v3 position;
    bool alive;
    v3 velocity;
    int _pad1;
    v3 orientation;
    int _pad2;
};

struct FireParticle {
    v3 position;
    bool alive;
    v3 velocity;
    float life;
    float life_drain;
    float _pad[3];
};

struct Dust {
    static const GLsizeiptr n_particles = 16384;

    gl::Program const* update_program;
    gl::Program const* draw_program;
    gl::Buffer<DustParticle> buffer;

    Dust(gl::Context &ctx, gl::Program const& update, gl::Program const& draw):
        update_program(&update),
        draw_program(&draw),
        buffer(n_particles, nullptr, gl::BufferType::Static)
    {
        this->update(ctx, true);
    }

    void update(gl::Context &ctx, float dt, bool init = false) {
        float seed = (float) rand() / (float) RAND_MAX;
        ctx.compute(*update_program, {
                { "init", init },
                { "seed", seed },
                { "dt", dt },
                { "dust_buf", buffer }
        }, buffer.size);
    }

    template<class T>
    void draw(T const& target, gl::Context &ctx, m4 view_matrix, gl::DrawParameters params) {
        ctx.draw_empty(target, *draw_program, gl::PrimitiveType::Points, buffer.size, params, {
                { "view", view_matrix },
                { "dust_buf", buffer },
        });
    }
};

struct Fire {
    static const GLsizeiptr n_particles = 70;

    gl::Texture2dArray const* tx;
    gl::Program const* update_program;
    gl::Program const* draw_program;
    m4 model_matrix = m4(1.0);
    gl::Buffer<FireParticle> buffer;

    Fire(Fire&& rhs) = default;
    Fire& operator=(Fire&&) = default;

    Fire(gl::Context &ctx, gl::Program const& update, gl::Program const& draw, gl::Texture2dArray const& tx, v3 pos = v3(0.0)):
        tx(&tx),
        update_program(&update),
        draw_program(&draw),
        buffer(n_particles, nullptr, gl::BufferType::Static)
    {
        model_matrix = translate(pos);
        this->update(ctx, true);
    }

    void update(gl::Context &ctx, float dt, bool init = false) {
        float seed = (float) rand() / (float) RAND_MAX;
        ctx.compute(*update_program, {
            { "init", init },
            { "seed", seed },
            { "dt", dt / 8.0f },
            { "fire_buf", buffer }
        }, buffer.size);
    }

    template<class T>
    void draw(T const& target, gl::Context &ctx, m4 view_matrix, gl::DrawParameters params) {
        params.program_point_size = true;
        params.enable_culling = false;
        params.enable_depth = true;
        params.depth_write = false;
        params.enable_blend = true;
        params.blend = { gl::Blend::One, gl::Blend::One };
        ctx.draw_empty(target, *draw_program, gl::PrimitiveType::Points, buffer.size, params, {
                { "model", model_matrix },
                { "view", view_matrix },
                { "fire_buf", buffer },
                { "tx", *tx },
        });
    }
};


int main(int argc, char* argv[]) {
    auto window = Window { "demo", 1280, 720 };
    window.set_vsync(0);

    //auto nk_ctx = nk_glfw3_init(window.window, NK_GLFW3_DEFAULT, 512 * 1024, 128 * 1024);
    //nk_font_atlas *atlas;
    //nk_glfw3_font_stash_begin(&atlas);
    //nk_glfw3_font_stash_end();

    auto ctx = gl::Context();
    auto params = gl::DrawParameters{};
    params.framebuffer_srgb = true;
    params.cubemap_seamless = true;
    params.depth_write = true;
    
    auto font = Font("resources/DejaVuSans.ttf");

    auto volumetric_scale = 4;

    auto volumetric_buffer = RenderTexture<1>(1280 / volumetric_scale, 720 / volumetric_scale, { GL_RGB16F }, false);
    auto surface_buffer = RenderTexture<1>(1280, 720, { GL_RGB16F }, false, 1, true );
    auto g_buffer = RenderTexture<4>(1280, 720, { GL_RGB8, GL_RGB8, GL_RGB16F, GL_RGB16F }, true, 1, false, true);

    auto scene = Scene {};
    scene.import_gltf("resources/sponza/Sponza.gltf");
    //scene.import_gltf("../../resources/forest.gltf");
    auto cube_maps = load_dds_cubemap("resources/output_skybox.dds");

    auto geom_program          = gl::Program::vert_frag("glsl/deferred_vert.glsl",  "glsl/deferred_frag.glsl");
    auto skybox_program        = gl::Program::vert_frag("glsl/cube_vert.glsl",      "glsl/skybox_frag.glsl");
    auto shadow_program        = gl::Program::vert_frag("glsl/deferred_vert.glsl",  "glsl/depth_frag.glsl");
    auto surface_light_program = gl::Program::vert_frag("glsl/fill_vert.glsl",      "glsl/surface_light.glsl");
    auto volume_light_program  = gl::Program::vert_frag("glsl/fill_vert.glsl",      "glsl/volume_light.glsl");
    auto cc_program            = gl::Program::vert_frag("glsl/fill_vert.glsl",      "glsl/cc_frag.glsl");

    auto dust_program = gl::Program::vert_frag("glsl/dust/vert.glsl", "glsl/dust/frag.glsl");
    auto dust_update = gl::Program { gl::Shader::from_file(GL_COMPUTE_SHADER, "glsl/dust/update.glsl") };
    Dust dust { ctx, dust_update, dust_program };

    auto fire_tx = gl::Texture2dArray::from_file("resources/fire_particles.png");
    auto fire_program = gl::Program::vert_frag("glsl/fire/vert.glsl", "glsl/fire/frag.glsl");
    auto fire_update = gl::Program { gl::Shader::from_file(GL_COMPUTE_SHADER, "glsl/fire/update.glsl") };

    std::vector<Fire> fires {};
    fires.emplace_back(ctx, fire_update, fire_program, fire_tx, v3(-4.95299, 0.99, 1.13686));
    fires.emplace_back(ctx, fire_update, fire_program, fire_tx, v3(-4.9545, 0.99, -1.75803));
    fires.emplace_back(ctx, fire_update, fire_program, fire_tx, v3(3.89948, 0.99, -1.75361));
    fires.emplace_back(ctx, fire_update, fire_program, fire_tx, v3(3.90465, 0.99, 1.14817));

    // TODO: abstract this to part of some light
    auto shadow_map = RenderTexture<0> { 2048 * 2, 2048 * 2, { }, true };
    shadow_map.depth->set_params(gl::SamplerParams::nice());
    auto light_inv = v3(0.2f, 2.0f, 0.1f).normalize();

    const float spot_light_intensity = 15.0f;
    for (float x = -1.f; x <= 1.f; x += 2) {
        for (float y = -1.f; y <= 1.f; y += 2) {
            scene.lights.spot_lights.elem(SpotLight{
                /*.position   =*/ v3(x * 9.5f - 0.2f, 3.3f, y * 3.5f - 0.2f),
                /*.range      =*/ 10.0,
                /*.colour     =*/ temp_to_rgb(4000.0f) * spot_light_intensity,
                /*.inner_cone =*/ std::cos(deg2rad(20.0f)),
                /*.forward    =*/ v3(0.0, 1.0, 0.0),
                /*.outer_cone =*/ std::cos(deg2rad(22.0f)),
            });
        }
    }

    scene.lights.light_probes.elem(LightProbe {
        /*.map_idx =*/ 0,
        /*.factor  =*/ 0.5,
    });

    const float sun_light_intensity = 15.0f;
    const float floor_light_intensity = sun_light_intensity * 0.5f;
    const float sky_light_intensity = sun_light_intensity * 0.6f;

    auto sun_light = SunLight {
        /*.direction      =*/ light_inv,
        /*.has_shadow     =*/ GL_TRUE,
        /*.colour         =*/ temp_to_rgb(5500.f) * sun_light_intensity, // TODO: add warmth in CC, use 6.5k here..
        /*.shadow_map_idx =*/ 0,
        /*.shadow_pv      =*/ m4(1.0)
    };
    auto sun_light_elem = scene.lights.sun_lights.elem(sun_light);

    scene.lights.tube_lights.elem(TubeLight {
        /*.position_a =*/ v3(6.f, 1.0f, -0.1f),
        /*.range      =*/ 10.0f,
        /*.position_b =*/ v3(-6.f, 1.0f, -0.1f),
        /*.radius     =*/ 0.3f,
        /*.colour     =*/ temp_to_rgb(4800.f) * floor_light_intensity,
    });

    scene.lights.tube_lights.elem(TubeLight {
        /*.position_a =*/ v3(5.f, 4.0f, -0.1f),
        /*.range      =*/ 10.0f,
        /*.position_b =*/ v3(-5.f, 4.0f, -0.1f),
        /*.radius     =*/ 0.3f,
        /*.colour     =*/ temp_to_rgb(5000.f) * sky_light_intensity,
    });

    auto camera = Camera{};
    camera.position = v3(-7.0f, 1.0f, 0.0f);

    // Combine the scene objects into a batch which we can render in one call.
    std::clog << "Building render batch\n";
    auto objs = scene.render_objects();
    auto batch = Batch<Vertex, Material>(objs.size());
    auto obj_buf = gl::Buffer<RenderObject<Vertex, Material>>(objs.size(), objs.data(), gl::BufferType::Static);
    batch.build(obj_buf, scene.storage.mesh.buf);

    float light_diff = 0.0001f;
    scene.update_graph();

    std::clog << "Starting main loop\n";
    auto prev_time = now();

    float frame_time = 16.0f;
    float fps = 1000.0f / frame_time;
    float arcball_dist = 2.0;

    while (!window.should_close()) {
        auto time = now();
        auto dt = (float) ns_to_ms(time - prev_time);
        frame_time = frame_time * 0.8f + dt * 0.2f;
        fps = 1000.0f / frame_time;
        dt = clamp(dt, 1.0f, 16.0f);
        prev_time = time;

        window.poll_events();
        //nk_glfw3_new_frame();
        while(auto ev = window.next_event()) {
            // Resize fbo textures when the window changes size
            if (ev->kind == WindowEvKind::FramebufferResize) {
                surface_buffer.size = window._view_size;
                g_buffer.size = window._view_size;
                volumetric_buffer.size = window._view_size / volumetric_scale;

                surface_buffer.init_fb();
                g_buffer.init_fb();
                volumetric_buffer.init_fb();
            }
            if (ev->kind == WindowEvKind::MouseWheelScroll) {
                arcball_dist = clamp(arcball_dist - (float) ev->mouse_wheel.y_off * 0.12f, 0.5f, 10.0f);
                //nk_gflw3_scroll_callback(window.window, ev->mouse_wheel.x_off, ev->mouse_wheel.y_off);
            }
            if (ev->kind == WindowEvKind::MouseButtonPress) {
                //nk_glfw3_mouse_button_callback(window.window, ev->mouse.button, GLFW_PRESS, ev->mouse.mods);
            }
            if (ev->kind == WindowEvKind::MouseButtonRelease) {
                //nk_glfw3_mouse_button_callback(window.window, ev->mouse.button, GLFW_RELEASE, ev->mouse.mods);
            }
        }

        if (window.key_pressed[GLFW_KEY_SPACE]) {
            std::cout << camera.position.x << ", " << camera.position.y << ", " << camera.position.z << "\n";
        }

        /*
        if (nk_begin(nk_ctx, "fps", nk_rect(0, 0, 110, 28), 0))
        {
            auto ss = std::stringstream{};
            ss << std::setprecision(4) << fps << " FPS";
            nk_layout_row_dynamic(nk_ctx, 10, 1);
            nk_label(nk_ctx, ss.str().c_str(), NK_TEXT_CENTERED);
        }
        nk_end(nk_ctx);
         */

        auto camera_speed = 0.002f * dt;
        if (window.key_pressed[GLFW_KEY_W]) camera.move_forward(camera_speed);
        if (window.key_pressed[GLFW_KEY_S]) camera.move_forward(-camera_speed);
        if (window.key_pressed[GLFW_KEY_A]) camera.move_left(camera_speed);
        if (window.key_pressed[GLFW_KEY_D]) camera.move_left(-camera_speed);

        if (window.mouse_pressed[0]) {
            camera.yaw += (float) window.mouse_diff.x * 0.002f;
            camera.pitch -= (float) window.mouse_diff.y * 0.002f;
        }
        //camera.arcball(v3(0.0), arcball_dist);
        camera.update();

        // Update particles
        dust.update(ctx, dt);
        for(auto& fire : fires) fire.update(ctx, dt);

        // Clear buffers
        window.clear(v4(0.0, 0.0, 0.0, 1.0), 1.0) ;
        g_buffer.clear({ v4(0.0, 0.0, 0.0, 1.0), v4(0.0, 0.0, 0.0, 0.0), v4(0.0, 0.0, 0.0, 0.0) }, 1.0);
        volumetric_buffer.clear({v4(0.0, 0.0, 0.0, 1.0)}, 1.0);
        shadow_map.clear({}, 1.0);

        // Draw the scene to the gbuffer
        params.enable_depth = true;
        params.enable_culling = true;
        params.depth_test = gl::DepthTest::Less;
        batch.draw(
            ctx, g_buffer, scene.storage.vert.buf, scene.storage.idx.buf, geom_program,
            gl::PrimitiveType::Triangles, params,
            {
                    { "view", camera.pv },
                    { "tx", scene.storage.textures },
                    { "eye_position", camera.position },
                    { "object_buf", obj_buf },
                    { "transform_buf", scene.transforms.buf },
                    { "attr_buf", scene.storage.material.buf },
                    { "tx_size_buf", scene.storage.tx_size.buf },
            });

        // Draw dust to the gbuffer.
        dust.draw(g_buffer, ctx, camera.pv, params);


        light_inv.z += light_diff;
        if (std::abs(light_inv.z) > 0.3) light_diff *= -1;
        light_inv = light_inv.normalize();
        auto depth_proj = ortho(-20.0f, 20.0f, -20.0f, 20.0f, -30.0f, 30.0f);
        auto depth_view = look_at(light_inv, v3(0, 0, 0), v3(0, 0, 1));
        auto depth_pv = depth_proj * depth_view;
        sun_light.direction = light_inv;
        sun_light.shadow_pv = depth_pv;
        scene.lights.sun_lights.set(sun_light_elem.index, sun_light);


        // Draw the scene to the shadow map.
        batch.draw(
            ctx, shadow_map, scene.storage.vert.buf, scene.storage.idx.buf, shadow_program,
            gl::PrimitiveType::Triangles, params,
            {
                { "view", depth_pv },
                { "transform_buf", scene.transforms.buf },
                { "object_buf", obj_buf },
                { "tx_size_buf", scene.storage.tx_size.buf },
            });

        gl::check_error("Drawing shadow");


        // Draw the skybox to the emission texture.
        params.depth_test = gl::DepthTest::LEqual;
        ctx.draw_empty(g_buffer, skybox_program, gl::PrimitiveType::TriangleStrip, 14, params, {
            { "view", camera.view },
            { "projection", camera.projection },
            { "cube_maps", cube_maps },
            { "env_map_idx", 0 },
        });


        // Lighting pass
        params.enable_depth = false;
        auto light_uniforms = gl::Uniforms {
            { "in_depth", *g_buffer.depth },
            { "in_col", *g_buffer.col[0] },
            { "in_emission", *g_buffer.col[1] },
            { "in_norm", *g_buffer.col[2] },
            { "in_mro", *g_buffer.col[3] },
            { "inv_perspective", camera.inv_projection },
            { "inv_view", camera.inv_view },
            { "shadow_maps", *shadow_map.depth },
            { "eye_position", camera.position },
            { "cube_maps", cube_maps },
            { "n_spot_lights", scene.lights.spot_lights.size() },
            { "n_tube_lights", scene.lights.tube_lights.size() },
            { "n_sun_lights", scene.lights.sun_lights.size() },
            { "n_probes", scene.lights.light_probes.size() },
            { "spot_light_buf", scene.lights.spot_lights.buf },
            { "tube_light_buf", scene.lights.tube_lights.buf },
            { "sun_light_buf" , scene.lights.sun_lights.buf },
            { "probe_buf", scene.lights.light_probes.buf },
        };
        ctx.draw_empty(surface_buffer, surface_light_program, gl::PrimitiveType::Triangles, 3, params, light_uniforms);
        ctx.draw_empty(volumetric_buffer, volume_light_program, gl::PrimitiveType::Triangles, 3, params, light_uniforms);

        // Draw fire to the surface light texture, uses additive blending.
        // Uses the gbuffer's depth s.t. fire particles are clipped by scene geometry.
        glNamedFramebufferTextureLayer(surface_buffer.fbo, GL_DEPTH_ATTACHMENT, *g_buffer.depth, 0, 0);
        for (auto& fire : fires) fire.draw(surface_buffer, ctx, camera.pv, params);

        // Draw FPS counter
        auto ss = std::stringstream{};
        ss << std::setprecision(4) << fps << " FPS";
        auto m = make_transform(v3(3.0, 1.0, -1.0), v3(1.0), r3::from_euler(v3(0, -M_PI / 2.0, 0)));
        font.draw_text(ss.str(), ctx, surface_buffer, params, camera.pv * m);

        // Generate mipmaps for weighted average depth & colour
        glGenerateTextureMipmap(*g_buffer.depth);
        glGenerateTextureMipmap(*surface_buffer.col[0]);

        // Post processing (DOF, HDR, CC, bloom, ...)
        ctx.draw_empty(window, cc_program, gl::PrimitiveType::Triangles, 3, params, {
            { "surface_light", *surface_buffer.col[0] },
            { "volume_light", *volumetric_buffer.col[0] },
            { "in_depth", *g_buffer.depth },
        });

        //nk_glfw3_render(NK_ANTI_ALIASING_ON);
        //ctx.invalidate(); // nuklear changes a bunch of state.

        window.display();
    }
}
