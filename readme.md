An OpenGL 4.6 renderer.

![sponza](sponza.png)

## Features:

 - Scene loading with GLTF.
 - PBR Lighting:
   - Metallic-rougnhess materials with cook-torrence BRDF.
   - Tube lights, directional lights, spot lights, and basic IBL.
   - Shadows for directional lights.
 - Volumetric lighting for directional & spotlights with shadowing.
 - Post-processing: Depth-of-field, HDR, bloom & tone mapping.
 - Uses multidraw indirect for lower driver overhead
 - API reduces statefullness for easier debugging: a draw call takes 
   a render target, program, uniforms & bindings, render state, and 
   the things to be drawn.


## TODO:

```
- tiled light culling
- follow view frustrum for directional shadow maps
- spot-light image projection
- shadow mapping for spot lights
- load lights & cameras from GLTF scene. (what about tube lights?)
- render to cube maps
- better vegitation (transmission etc?)
- skeletal animation
- particles
  - falling leaves?
- GPU fluid? flames etc
- flow field water? :3

- precomputed radiance transfer?
 - http://developer.amd.com/wordpress/media/2013/01/Chapter01-Chen-Lighting_and_Material_of_Halo3.pdf

  https://www.shadertoy.com/view/4ttSWf
  http://www.michaelwalczyk.com/blog/2017/5/25/ray-marching
  http://www.cse.chalmers.se/~uffe/ClusteredWithShadows.pdf
```
