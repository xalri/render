#pragma once

#include <array>
#include <iostream>
#include <cstring>
#include "common.hpp"
#include "buffer.hpp"

namespace gl {

template<class T, size_t N>
struct RingBuffer;

/// A slice of a ring buffer. The data pointer must not be saved.
template<class T, size_t N>
struct Slice {
    Slice(Slice &&other) : data(other.data), idx(other.idx), multi(other.multi) {
        other.multi = nullptr;
        other.data = nullptr;
    };

    Slice(Slice const &) = delete;

    Slice &operator=(Slice const &) = delete;

    Slice &operator=(Slice &&) = delete;

    Slice(T *data, size_t idx, RingBuffer<T, N> *multi) : data(data), idx(idx), multi(multi) {}

    ~Slice();

    /// Implicit cast to the pointer type.
    operator T *() {
        return data;
    }

    /// Get the offset pointer from the start of the ring buffer.
    uint8_t *offset() {
        return (uint8_t *) ((size_t) (data) - (size_t) (multi->data));
    }

    /// Bind the buffer with this slice's offset. After binding, the slice can no longer
    /// be written to, but must be kept alive until after the draw calls which use it.
    void bind(GLenum target, GLuint index) const {
        bound = true;
        assert(offset() == idx * multi->size * sizeof(T)); // TODO: remove after testing
        glBindBufferRange(target, index, multi->buf.id, offset(), multi->size * sizeof(T));
        check_error("RingBuffer::bind");
    }

private:
    T *data;
    size_t idx;
    RingBuffer<T, N> *multi;
    bool bound = false;
};

template<class T, size_t N>
struct RingBuffer {
    Buffer<T> buf;
    size_t size;
    size_t current_slice = 0;
    std::array<GLsync, N> fences{};

    /// Create a multibuffer where each of the `N` sections holds `size` elements of type T.
    explicit RingBuffer(size_t size) :
            size(size),
            buf(N * size, nullptr, BufferType::MappedRW) {}

    /// Get the next section of the multibuffer. Waits on fences if required.
    Slice<T, N> get() {
        if (fences[current_slice]) {
            glClientWaitSync(fences[current_slice], GL_SYNC_FLUSH_COMMANDS_BIT, 1'000'000'000);
            check_error("RingBuffer::get glClientWaitSync");
            fences[current_slice] = 0;
        }
        auto next_slice = current_slice == N - 1 ? 0 : current_slice + 1;
        // buf.data is a T*, so no need to multiply by sizeof(T)
        auto slice = Slice<T, N>(buf.data + (current_slice * size), current_slice, this);
        current_slice = next_slice;
        return slice;
    }

    GLuint id() { return buf; }
};

template<class T, size_t N>
Slice<T, N>::~Slice() {
    if (!multi) return;
    // Add a fence for the used section of the multi buffer.
    multi->fences[idx] = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    check_error("Slice::~Slice glFenceSync");
}

/// A reference to some component which is accessible on the GPU
template<class T>
struct Elem {
    uint32_t index;
};

/// A reference to a block of components on the GPU.
template<class T>
struct View {
    uint32_t first_index;
    uint32_t count;
};

enum class AllocKind { Dynamic, Deferred, Mapped };

template<class T>
struct Alloc {
    Buffer<T> buf;
    std::vector<T> pre_buffer {};
    uint32_t head = 0;
    bool is_deferred = false;
    bool is_mapped = false;

    explicit Alloc(size_t size, AllocKind kind = AllocKind::Dynamic) :
            buf(size, nullptr, kind == AllocKind::Mapped ? BufferType::MappedRW : BufferType::Dynamic),
            is_deferred(kind == AllocKind::Deferred),
            is_mapped(kind == AllocKind::Mapped)
    { }

    uint32_t size() {
        return head;
    }

    /*
    explicit Alloc(bool deferred, size_t size, BufferType type) :
        buf(size, nullptr, type),
        is_deferred(deferred)
        { }
        */

        /*
    static Alloc dynamic(size_t max_size) {
        return Alloc(false, max_size, BufferType::Dynamic);
    }

    static Alloc transient(size_t max_size) {
        return Alloc(false, max_size, BufferType::MappedWrite);
    }

    static Alloc deferred() {
        return Alloc(true, 0, BufferType::Static);
    }

    void reify() {
        if (!is_deferred) {
            std::cerr << "Attempted to reify a non-deferred Alloc\n";
            exit(1);
        }
        buf = Buffer<T>{ pre_buffer.size(), pre_buffer.data(), BufferType::Static };
        is_deferred = false;
        pre_buffer = std::vector<T>{};
    }

    void resize() {

    }
         */

    /// Allocate a block of GPU memory for storing a single element type T.
    Elem<T> elem(T value, bool reify = true) {
        // TODO: always push to deferred, maybe reify immediately
        auto elem = Elem<T> { head };
        head += 1;
        if (head > buf.size) {
            std::cerr << "Attempted to allocate past the end of the buffer" << std::endl;
            exit(1);
        }
        set(elem.index, value);
        return elem;
    }

    /// Allocate a block of GPU memory for storing elements type T.
    View<T> view(std::vector<T> const &elements, bool reify = true) {
        auto n = elements.size();
        auto view = View<T> { head, (uint32_t) n };
        head += n;
        if (head > (uint32_t) buf.size) {
            std::cerr << "Attempted to allocate past the end of the buffer" << std::endl;
            exit(1);
        }
        set_range(view.first_index, n, elements.data());
        return view;
    }

    /// Set the value of some range of elements
    void set_range(size_t first, size_t n, T const* value) {
        if (is_mapped) {
            assert(buf.data != nullptr);
            std::memcpy(buf.data + first, value, n * sizeof(T));
            //glFlushMappedNamedBufferRange(buf.id, )
        } else {
            glNamedBufferSubData(buf, first * sizeof(T), n * sizeof(T), value);
            check_error("Alloc::set_range bufferSubData");
        }
    }

    /// Set the value of some element
    void set(size_t idx, T value) {
        set_range(idx, 1, &value);
    }
};
}
