#pragma once

#include "common.hpp"
#include "object.hpp"

namespace gl {

enum class BufferType {
    Static,
    Dynamic,
    MappedRead,
    MappedWrite,
    MappedRW,
};

/// A GPU buffer.
template <typename T>
struct Buffer: public Object  {
    GLsizeiptr size = 0;
    T* data = nullptr;

    Buffer(Buffer&&) = default;
    Buffer& operator=(Buffer&&) = default;

    Buffer() = default;

    Buffer(GLsizeiptr size, const T *initial_data, BufferType kind, bool read = false, bool write = false): size(size), data(nullptr) {
        if (size == 0) return;

        GLbitfield flags = 0;
        if (kind == BufferType::Dynamic) flags |= GL_DYNAMIC_STORAGE_BIT;
        if (kind == BufferType::MappedRead) flags |= GL_MAP_READ_BIT;
        if (kind == BufferType::MappedWrite) flags |= GL_MAP_WRITE_BIT;
        if (kind == BufferType::MappedRW) flags |= GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;

        auto mapped = kind == BufferType::MappedRead || kind == BufferType::MappedWrite || kind == BufferType::MappedRW;
        if (mapped) flags |= GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT;

        checked(glCreateBuffers(1, &id));
        checked(glNamedBufferStorage(id, size * sizeof(T), initial_data, flags));

        if (mapped) checked(data = (T*) glMapNamedBufferRange(id, 0, size * sizeof(T), flags));
    }

    /// Get the size of the buffer in bytes
    size_t bytes() {
        return size * sizeof(T);
    }

    ~Buffer() {
        if (id > 0) checked(glDeleteBuffers(1, &id));
        id = 0;
    }

    void bind(GLenum target) const {
        checked(glBindBuffer(target, id));
    }

    void bind(GLenum target, GLuint index) const {
        checked(glBindBufferBase(target, index, id));
        check_error("Buffer::bind");
    }

    void bind_range(GLenum target, GLuint index, size_t offset, size_t count) const {
        glBindBufferRange(target, index, id, offset, count * sizeof(T));
        check_error("Buffer::bind_range");
    }
};

}