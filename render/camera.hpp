#pragma once

// windows compatibility crap:
#undef near
#undef far

#include <array>
#include "xal_math.h"

struct Camera {
    float fovy = deg2rad(40.0f);
    float aspect = 16.0f / 9.0f;
    float pitch = 0.0f;
    float yaw = 0.0f;
    float near = 0.01f;
    float far = 100.0f;
    v3 position = v3(0.0f, 0.0f, 0.0f);
    v3 up = v3(0.0f, 1.0f, 0.0f);

    // derived
    v3 front = v3(0.0f, 0.0f, 1.0f);
    m4 projection = m4(1.0);
    m4 inv_projection = m4(1.0);
    m4 view = m4(1.0);
    m4 inv_view = m4(1.0);
    m4 pv = m4(1.0);
    m4 inv_pv = m4(1.0);

    void update_front() {
        front = (v3(std::cos(pitch) * std::cos(yaw), std::sin(pitch), std::cos(pitch) * std::sin(yaw))).normalize();
    }

    void arcball(v3 target, float distance) {
        v3 front = (v3(std::cos(pitch) * std::cos(yaw), std::sin(pitch), std::cos(pitch) * std::sin(yaw))).normalize();
        position = target - (front * distance);
    }

    void update_matrices() {
        projection = perspective(fovy, aspect, near, far);
        inv_projection = projection.invert();
        view = look_at(position, position + front, up);
        inv_view = view.invert();
        pv = projection * view;
        inv_pv = pv.invert();
    }

    void update() {
        update_front();
        update_matrices();
    }

    /// Get the world-space points of the camera's frustrum.
    std::array<v4, 8> frustrum_points() {
        auto points = std::array<v4, 8> {
            v4 { -1.f, -1.f, 0.f, 1.f },
            v4 { -1.f,  1.f, 0.f, 1.f },
            v4 {  1.f,  1.f, 0.f, 1.f },
            v4 {  1.f, -1.f, 0.f, 1.f },
            v4 { -1.f, -1.f, 1.f, 1.f },
            v4 { -1.f,  1.f, 1.f, 1.f },
            v4 {  1.f,  1.f, 1.f, 1.f },
            v4 {  1.f, -1.f, 1.f, 1.f }
        };

        for(auto i = 0; i < 8; i++) {
            points[i] = inv_pv * points[i];
            points[i] = points[i] / points[i].w;
        }

        return points;
    }

    void project_frustrum(v3 dir) {
        auto points = frustrum_points();
        auto max = v3(std::numeric_limits<float>::max());
        auto min = -max;

        for(auto p : points) {
            min.x = std::min(min.x, p.x);
            // ...
        }
    }

    void move_forward(float distance) {
        position = position + (front * distance);
    }

    void move_left(float distance) {
        position = position - (front.cross(up).normalize() * distance);
    }
};