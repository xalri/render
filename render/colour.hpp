#pragma once

#include "common.hpp"
#include "xal_math.h"

// Colour temperature in kelvin to approx RGB.
// http://advances.realtimerendering.com/s2018/Siggraph%202018%20HDRP%20talk_with%20notes.pdf
v3 temp_to_rgb(float temp) {
    temp = clamp(temp, 1000.f, 40000.f) / 1000.f;
    float temp2 = temp * temp;

    float r = (temp < 6.570f) ? 1.0f : (1.35651f + 0.216422f * temp + 0.000633715f * temp2) / (-3.24223f + 0.918711f * temp);
    float g = (temp < 6.570f) ?
              (-399.809f + 414.271f * temp + 111.543f * temp2) / (2779.24f + 164.143f * temp + 84.7356f * temp2) :
              (1370.38f + 734.616f * temp + 0.689955f * temp2) / (-4625.69f + 1699.87f * temp);
    float b = (temp > 6.570f) ? 1.0f : (348.963f - 523.53f * temp + 183.62f * temp2) / (2848.82f - 214.52f * temp + 78.8614f * temp2);

    return v3(r, g, b);
}
