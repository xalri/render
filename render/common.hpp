#pragma once

#include <glad/glad.h>
#include "xal_util.h"
#include "xal_math.h"

/// Wraps OpenGL 4.5 with objects and error checking.
namespace gl {

/// Check for an OpenGL error
void check_error(const char *stmt);

/// Wraps some statement with OpenGL error checking.
#define checked(stmt) { stmt; gl::check_error(#stmt); }

/// Get the GL primitive type for some c++ type.
template<class T> GLenum type();

#define impl_type(t, v) template<> inline GLenum type<t>() { return v; }

impl_type(float, GL_FLOAT);
impl_type(int32_t, GL_INT);
impl_type(int16_t, GL_SHORT);
impl_type(int8_t, GL_BYTE);
impl_type(uint32_t, GL_UNSIGNED_INT);
impl_type(uint16_t, GL_UNSIGNED_SHORT);
impl_type(uint8_t, GL_UNSIGNED_BYTE);
impl_type(v2, GL_FLOAT);
impl_type(v3, GL_FLOAT);
impl_type(v4, GL_FLOAT);
impl_type(m3, GL_FLOAT);
impl_type(m4, GL_FLOAT);

#undef impl_type

/// Get the number of primitives which make up some scalar/vector/matrix type.
template<class T> size_t n_elements();

#define impl_n_elements(t, n) template<> inline size_t n_elements<t>() { return n; }

impl_n_elements(int8_t, 1);
impl_n_elements(int16_t, 1);
impl_n_elements(uint16_t, 1);
impl_n_elements(float, 1);
impl_n_elements(v2, 2);
impl_n_elements(v3, 3);
impl_n_elements(v4, 4);
impl_n_elements(m3, 9);
impl_n_elements(m4, 16);

#undef impl_n_elements

} // namespace gl
