#include "context.hpp"

namespace gl {
Context::Context() : vao_type { typeid(void) } {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
}
}
