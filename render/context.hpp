#pragma once

#include <typeindex>
#include "common.hpp"
#include "buffer.hpp"
#include "shader.hpp"
#include "vertex_format.hpp"

using IdxType = uint16_t;
//using IdxType = uint32_t;

namespace gl {

enum class Blend: GLenum {
    One = GL_ONE,
    SrcAlpha = GL_SRC_ALPHA,
    OneMinusSrcAlpha = GL_ONE_MINUS_SRC_ALPHA
};

enum class CullFace: GLenum { Back = GL_BACK, Front = GL_FRONT };

enum class FaceWinding: GLenum { Ccw = GL_CCW, Cw = GL_CW };

enum class DepthTest: GLenum {
    Never = GL_NEVER,
    Less = GL_LESS,
    Equal = GL_EQUAL,
    LEqual = GL_LEQUAL,
    Greater = GL_GREATER,
    NotEqual = GL_NOTEQUAL,
    GEqual = GL_GEQUAL,
    Always = GL_ALWAYS,
};

enum class PrimitiveType: GLenum {
    Points = GL_POINTS,
    Triangles = GL_TRIANGLES,
    TriangleStrip = GL_TRIANGLE_STRIP,
    Lines = GL_LINES,
};

struct DrawParameters {
    DepthTest depth_test = DepthTest::Less;
    bool depth_write = false;
    bool enable_depth = false;
    bool enable_blend = false;
    bool enable_culling = false;
    bool program_point_size = false;
    bool framebuffer_srgb = false;
    bool cubemap_seamless = false;
    CullFace cull_face = CullFace::Back;
    std::pair<Blend, Blend> blend = { Blend::SrcAlpha, Blend::OneMinusSrcAlpha };
};

#define gl_set(feature, enabled) { if (enabled) glEnable(feature); else glDisable(feature); }

struct DrawElementsCmd {
    uint32_t count;
    uint32_t instance_count;
    uint32_t first_index;
    uint32_t base_vertex;
    uint32_t base_instance;
};

struct DrawArraysCmd {
    uint32_t count;
    uint32_t instance_count;
    uint32_t first_vertex;
    uint32_t base_instance;
};

/// Tracks OpenGL state to avoid unnecessary API calls.
struct Context {
    DrawParameters params{};
    v2i view_offset{};
    v2i view_size{};

    GLuint vao = 0;
    std::type_index vao_type;
    gl::VertexFormat fmt{};
    GLuint vao_elem_buffer = 0;
    GLuint vao_vert_buffer = 0;
    GLuint draw_indirect = 0;
    GLuint dispatch_indirect = 0;
    std::array<GLuint, 64> uniform_buffers{};
    std::array<GLuint, 64> storage_buffers{};
    // TODO: track bound textures

    bool params_valid = false;
    bool view_valid = false;
    bool vao_valid = false;

    void invalidate() {
        params_valid = false;
        view_valid = false;
        vao_valid = false;
    }

    Context();

    void set_view(v2i offset, v2i size) {
        if (!view_valid || offset != view_offset || size != view_size) glViewport(offset.x, offset.y, size.x, size.y);
        view_valid = true;
        view_offset = offset;
        view_size = size;
    }

    void set_params(DrawParameters const &p) {
        if (!params_valid || p.depth_test != params.depth_test) glDepthFunc((GLenum) p.depth_test);
        if (!params_valid || p.depth_write != params.depth_write) glDepthMask((GLboolean) p.depth_write);
        if (!params_valid || p.enable_depth != params.enable_depth) gl_set(GL_DEPTH_TEST, p.enable_depth);
        if (!params_valid || p.program_point_size != params.program_point_size) gl_set(GL_PROGRAM_POINT_SIZE, p.program_point_size);
        if (!params_valid || p.enable_blend != params.enable_blend) gl_set(GL_BLEND, p.enable_blend);
        if (!params_valid || p.enable_culling != params.enable_culling) gl_set(GL_CULL_FACE, p.enable_culling);
        if (!params_valid || p.framebuffer_srgb != params.framebuffer_srgb) gl_set(GL_FRAMEBUFFER_SRGB, p.framebuffer_srgb);
        if (!params_valid || p.cubemap_seamless != params.cubemap_seamless) gl_set(GL_TEXTURE_CUBE_MAP_SEAMLESS, p.cubemap_seamless);
        if (!params_valid || p.cull_face != params.cull_face) glCullFace((GLenum) p.cull_face);
        if (!params_valid || p.blend != params.blend) glBlendFunc((GLenum) p.blend.first, (GLenum) p.blend.second);

        gl::check_error("Context::set_params");
        params = p;
        params_valid = true;
    }

    template<class V>
    void set_vertex_buffer(gl::Buffer<V> const &vert, gl::Buffer<IdxType> const &elem) {
        if (!vao_valid) glBindVertexArray(vao);

        auto vert_changed = !vao_valid || vao_vert_buffer != vert;
        auto elem_changed = !vao_valid || vao_elem_buffer != elem;
        auto type_changed = !vao_valid || vao_type != typeid(V);
        auto format = gl::vertex_format<V>();

        if (elem_changed) {
            checked(glVertexArrayElementBuffer(vao, elem));
            vao_elem_buffer = elem;
        }

        if (vert_changed || type_changed) {
            checked(glVertexArrayVertexBuffer(vao, 0, vert, 0, format.stride));
            vao_vert_buffer = vert;
        }

        if (type_changed) {
            vao_type = typeid(V);
            GLuint stride = 0;
            for (auto i = 0u; i < format.n; i++) {
                auto attr = format.attrs[i];
                checked(glEnableVertexArrayAttrib(vao, i));
                checked(glVertexArrayAttribFormat(vao, i, attr.elements, attr.kind, attr.normalized, stride));
                checked(glVertexArrayAttribBinding(vao, i, 0));
                stride += attr.size;
            }
        }
        vao_valid = true;
    }

    /// Prepare for a draw call.
    template <class Target>
    void pre_draw(Target const& target, gl::Program const& program, DrawParameters const& params, Uniforms const& uniforms) {
        target.bind_framebuffer();
        target.set_draw_buffers();
        set_view(target.view_offset(), target.view_size());
        set_params(params);
        program.use();
        program.set_uniforms(uniforms);
        gl::check_error("Context::pre_draw");
    }

    void compute(gl::Program const& program, Uniforms const& uniforms, size_t x, size_t y = 1, size_t z = 1) {
        program.use();
        program.set_uniforms(uniforms);
        glDispatchCompute(x, y, z);
        gl::check_error("Context::compute");
    }

    /// Draw with no geometry.
    template <class Target>
    void draw_empty(
            Target const& target,
            gl::Program const& program,
            PrimitiveType prim_type,
            GLsizei count,
            DrawParameters const& params,
            Uniforms const& uniforms)
    {
        pre_draw(target, program, params, uniforms);
        checked(glDrawArrays((GLenum) prim_type, 0, count));
    }

    /// Draw some geometry.
    template <class V, class Target>
    void draw(
            gl::Buffer<V> const &vert,
            gl::Buffer<GLushort> const &elem,
            Target const& target,
            gl::Program const& program,
            PrimitiveType prim_type,
            GLsizei count,
            DrawParameters const& params,
            Uniforms const& uniforms)
    {
        set_vertex_buffer(vert, elem);
        draw_empty(target, program, prim_type, count, params, uniforms);
    }

    template <class V, class Target>
    void draw_elems(
            gl::Buffer<V> const &vert,
            gl::Buffer<GLushort> const &elem,
            Target const& target,
            gl::Program const& program,
            PrimitiveType prim_type,
            GLsizei count,
            GLsizei idx_offset,
            GLsizei base_vertex,
            DrawParameters const& params,
            Uniforms const& uniforms)
    {
        pre_draw(target, program, params, uniforms);
        checked(glDrawElementsInstancedBaseVertexBaseInstance(
                (GLenum) prim_type, count, gl::type<IdxType>(),
                (void*) (idx_offset * sizeof(IdxType)), 1, base_vertex, 0));
    }

};
}
