#pragma once

#include <gli/gli.hpp>
#include "common.hpp"
#include "texture.hpp"

// - filtering environment maps for IBL
// - functions for drawing to a cube map
// - ..

gl::TextureCubeArray load_dds_cubemap(const char* filename) {
    std::clog << "Loading cubemap '" << filename << "'\n";
    auto image = gli::load_dds(filename);
    gli::gl GL(gli::gl::PROFILE_GL33);
    gli::gl::format const cube_fmt = GL.translate(image.format(), image.swizzles());

    auto cube_map = gl::TextureCubeArray(image.levels(), cube_fmt.Internal, image.extent(0).x, image.extent(0).y);
    cube_map.set_params(gl::SamplerParams::nice());

    auto is_compressed = gli::is_compressed(image.format());

    for (auto face = 0u; face < image.faces(); ++face) {
        for (auto level = 0u; level < image.levels(); ++level) {
            auto extent = image.extent(level);
            if (is_compressed) {
                glCompressedTextureSubImage3D(
                        cube_map, level, 0, 0, face, extent.x, extent.y, extent.z,
                        cube_fmt.Internal, image.size(level), image.data(0, face, level));
            } else {
                glTextureSubImage3D(
                        cube_map, level, 0, 0, face, extent.x, extent.y, extent.z,
                        cube_fmt.External, cube_fmt.Type, image.data(0, face, level));
            }
            gl::check_error("load_dds_cubemap textureSubImage");
        }
    }

    return cube_map;
}
