#define DEBUG_DRAW_IMPLEMENTATION
#include "debug.hpp"

const char* DEBUG_VERT_SOURCE = R"""(
    #version 450 core

    in vec3 in_pos;
    in vec3 in_col;
    in float point_size;

    out vec4 v_col;
    uniform mat4 pv;

    void main() {
        gl_Position = pv * vec4(in_pos, 1.0);
        gl_PointSize = point_size;
        v_col = vec4(in_col, 0.3);
    }
)""";

const char* DEBUG_FRAG_SOURCE = R"""(
    #version 450 core

    in vec4 v_col;
    out vec4 out_col;

    void main() { out_col = v_col; }
)""";
