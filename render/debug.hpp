#pragma once

#include "xal_math.h"
#include "camera.hpp"
#include "buffer.hpp"
#include "context.hpp"
#include <cstring>

// #define DEBUG_DRAW_MAX_LINES 131072
// #define DEBUG_DRAW_VERTEX_BUFFER_SIZE 32768

#define DEBUG_DRAW_CXX11_SUPPORTED 1
using ddVec3 = v3;
using ddVec3_In = v3 const&;
using ddVec3_Out = v3&;
#define DEBUG_DRAW_VEC3_TYPE_DEFINED 1

using ddMat4x4 = m4;
using ddMat4x4_In = m4 const&;
using ddMat4x4_Out = m4&;
#define DEBUG_DRAW_MAT4X4_TYPE_DEFINED 1

#include "debug_draw.hpp"

template<> inline gl::VertexFormat gl::vertex_format<dd::DrawVertex>() {
    return gl::VertexFormat::make<3>({ gl::attr<v3>(), gl::attr<v3>(), gl::attr<float>() });
}

extern const char *DEBUG_VERT_SOURCE;
extern const char *DEBUG_FRAG_SOURCE;

template<class Target>
struct DDRender: dd::RenderInterface {
    gl::Context &ctx;
    gl::Program prog;
    m4 pv { 1.0 };
    Target* target = nullptr;
    gl::DrawParameters params {};
    gl::Buffer<dd::DrawVertex> verts { DEBUG_DRAW_VERTEX_BUFFER_SIZE, nullptr, gl::BufferType::MappedWrite };
    gl::Buffer<IdxType> elems{}; // empty elem buffer
    GLsync fence = nullptr;

    DDRender(gl::Context& ctx):
        ctx(ctx),
        prog(gl::Program{ gl::Shader(GL_VERTEX_SHADER, DEBUG_VERT_SOURCE), gl::Shader(GL_FRAGMENT_SHADER, DEBUG_FRAG_SOURCE) })
    {
        params.enable_depth = true;
        params.depth_write = true;
        params.enable_blend = true;
    }

    virtual void drawPointList(dd::DrawVertex const* points, int count, bool depthEnabled) {
        if (target == nullptr) return;
        params.enable_depth = depthEnabled;
        if (fence) glClientWaitSync(fence, GL_SYNC_FLUSH_COMMANDS_BIT, 1'000'000'000);
        std::memcpy(verts.data, points, count * sizeof(dd::DrawVertex));
        ctx.draw(verts, elems, *target, prog, gl::PrimitiveType::Points, count, params, {
            { "pv", pv }
        } );
        fence = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    }

    virtual void drawLineList(dd::DrawVertex const* lines, int count, bool depthEnabled) {
        if (target == nullptr) return;
        params.enable_depth = depthEnabled;
        if (fence) glClientWaitSync(fence, GL_SYNC_FLUSH_COMMANDS_BIT, 1'000'000'000);
        std::memcpy(verts.data, lines, count * sizeof(dd::DrawVertex));
        ctx.draw(verts, elems, *target, prog, gl::PrimitiveType::Lines, count, params, {
            { "pv", pv }
        } );
        fence = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    }
};