
#include "draw.hpp"

const char* BATCH_COMPUTE_SHADER = R"""(
    #version 450 core
    #extension GL_ARB_shader_draw_parameters : enable

    layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

    struct Object {
        uint mesh;
        uint transform;
        uint attrs;
    };

    struct Mesh {
        vec3 centre;
        float radius;
        uint first_vert;
        uint count;
        uint first_idx;
        uint idx_count;
    };

    struct DrawCmd {
        uint count;
        uint instance_count;
        uint first_index;
        uint base_vertex;
        uint base_instance;
    };

    layout(std430, binding = 0) buffer _a { Object objects[]; };
    layout(std430, binding = 1) buffer _b { Mesh meshes[]; };
    layout(std430, binding = 2) buffer _c { DrawCmd cmd[]; };

    void main() {
        uint id = gl_GlobalInvocationID.x;
        Mesh mesh = meshes[objects[id].mesh];
        cmd[id] = DrawCmd(mesh.idx_count, 1, mesh.first_idx, mesh.first_vert, 0);
    }
)""";
