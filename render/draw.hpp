#pragma once

#include "common.hpp"
#include "buffer.hpp"
#include "shader.hpp"
#include "alloc.hpp"
#include "context.hpp"
#include <typeindex>
#include <optional>

/// Data for a mesh. Gives the bounding circle for frustrum culling and the offsets into the
/// index and element buffers.
template<class T>
struct MeshData {
    v3 centre;
    float radius;
    gl::View<T> verts;
    gl::View<IdxType> indices;
};

/// An object to be batch rendered, with vertex attributes type V and material type M.
/// Contains indices into the mesh data, transform, and material buffers.
template<class V, class M>
struct RenderObject {
    gl::Elem<MeshData<V>> mesh;
    gl::Elem<m4> transform;
    gl::Elem<M> material;
};

extern const char *BATCH_COMPUTE_SHADER;

/// A batch of render commands for sets of objects with the same vertex attribute type & shader.
/// The draw list is built once with GPU frustum culling for each view frustrum & set of object
/// transforms, then can run multiple times with different shaders.
template<class V, class O>
struct Batch {
    GLuint size = 0;
    gl::Buffer<gl::DrawElementsCmd> cmd_buf;
    gl::Program prog;

    /// Create a new batch with the given maximum size.
    explicit Batch(size_t max_size = 16384) :
            cmd_buf(max_size, nullptr, gl::BufferType::Static),
            prog({gl::Shader(GL_COMPUTE_SHADER, BATCH_COMPUTE_SHADER)}) {}

    // TODO: take view frustrum, GPU clipping
    void build(gl::Buffer<RenderObject<V, O>> const &objects, gl::Buffer<MeshData<V>> const &meshes) {
        size = (GLuint) objects.size;

        objects.bind(GL_SHADER_STORAGE_BUFFER, 0);
        meshes.bind(GL_SHADER_STORAGE_BUFFER, 1);
        cmd_buf.bind(GL_SHADER_STORAGE_BUFFER, 2);

        prog.use();
        glDispatchCompute(size, 1, 1);
        gl::check_error("Batch::build");
    }

    template <class Target>
    void draw(
            gl::Context & ctx,
            Target const& target,
            gl::Buffer<V> const &vert,
            gl::Buffer<GLushort> const &elem,
            gl::Program const& program,
            gl::PrimitiveType prim_type,
            gl::DrawParameters const& params,
            gl::Uniforms const& uniforms)
    {
        ctx.pre_draw(target, program, params, uniforms);
        ctx.set_vertex_buffer(vert, elem);
        cmd_buf.bind(GL_DRAW_INDIRECT_BUFFER);
        glMemoryBarrier(GL_COMMAND_BARRIER_BIT);
        glMultiDrawElementsIndirect((GLenum) prim_type, gl::type<IdxType>(), 0, size, 0);
        gl::check_error("Batch::draw");
    }
};

/// A stack of 2D textures which can be drawn to.
template<size_t N>
struct RenderTexture : private non_movable/*, public gl::RenderTarget*/ {
    std::array<std::optional<gl::Texture2dArray>, N> col{};
    std::optional<gl::Texture2dArray> depth{};
    v2i size = v2i(0);
    GLuint layer = 0;
    GLuint fbo = 0;
    std::array<GLenum, N> internal_format;
    bool has_depth = false;
    GLuint layers = 1;
    bool col_mip = false;
    bool depth_mip = false;

    void validate() {
        auto valid = glCheckNamedFramebufferStatus(fbo, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
        if (!valid) {
            std::cerr << "Failed to create framebuffer\n";
            exit(1);
        }
    }

    void init_fb() {
        if (fbo > 0) { delete_fb(); }
        glCreateFramebuffers(1, &fbo);
        auto mip_levels = (int) std::log2((float)std::max(size.x, size.y));

        for(auto i = 0; i < N; i++)
            col[i] = { gl::Texture2dArray(col_mip ? mip_levels : 1, internal_format[i], size.x, size.y, layers) };
        if (has_depth)
            depth = { gl::Texture2dArray(depth_mip ? mip_levels : 1, GL_DEPTH_COMPONENT32, size.x, size.y, layers) };
        gl::check_error("RenderTexture::init_fb");

        apply_layer();
        validate();
    }

    void apply_layer() const {
        for(auto i = 0; i < N; i++)
            glNamedFramebufferTextureLayer(fbo, (GLenum)(GL_COLOR_ATTACHMENT0 + i), *col[i], 0, layer);
        if (has_depth)
            glNamedFramebufferTextureLayer(fbo, GL_DEPTH_ATTACHMENT, *depth, 0, layer);
        gl::check_error("RenderTexture::apply_layer");
    }

    void clear(std::array<v4, N> clear_col, float clear_depth) {
        apply_layer();
        for (auto i = 0; i < N; i++) glClearNamedFramebufferfv(fbo, GL_COLOR, i, &(clear_col[i].x));
        gl::check_error("RenderTexture::clear colour");
        glClearNamedFramebufferfv(fbo, GL_DEPTH, 0, &clear_depth);
        gl::check_error("RenderTexture::clear depth");
    }

    void delete_fb() {
        glDeleteFramebuffers(1, &fbo);
        fbo = 0;
        gl::check_error("RenderTexture::delete_fb");
    }

    RenderTexture(
            GLsizei w,
            GLsizei h,
            std::array<GLenum, N> internal_format,
            bool has_depth = true,
            GLuint layers = 1,
            bool col_mip = false,
            bool depth_mip = false
    ): size(w, h), layer(0), internal_format(internal_format), has_depth(has_depth), layers(layers), col_mip(col_mip), depth_mip(depth_mip)
    {
        init_fb();
    }

    ~RenderTexture() {
        delete_fb();
    }

    /// Bind the framebuffer for drawing.
    void bind_framebuffer() const {
        apply_layer();
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
        gl::check_error("RenderTexture::bind");
    }

    v2i view_size() const { return size; }

    v2i view_offset() const { return v2i(0, 0); }

    void set_draw_buffers() const {
        std::array<GLenum, N> draw_buffers{};
        for (auto i = 0; i < N; i++) draw_buffers[i] = (GLenum)(GL_COLOR_ATTACHMENT0 + i);
        glNamedFramebufferDrawBuffers(fbo, N, draw_buffers.data());
        gl::check_error("RenderTexture::set_draw_buffers");
    }
};

/// A double-buffered FBO for post-processing.
struct DoubleBuffer : private non_movable {
    RenderTexture<1> a, b;
    bool swapped = false;

    void init_fb(v2i size) {
        a.size = size;
        b.size = size;
        a.init_fb();
        b.init_fb();
    }

    void delete_fb() {
        a.delete_fb();
        b.delete_fb();
    }

    DoubleBuffer(GLsizei width, GLsizei height, GLenum internal_format, bool _has_depth) :
            a(width, height, {internal_format}, _has_depth),
            b(width, height, {internal_format}, _has_depth) {}

    /// Get the current FBO.
    inline GLuint get_fbo() {
        return swapped ? b.fbo : a.fbo;
    }

    /// Clear the current buffer.
    inline void clear(v4 clear_col, float clear_depth) {
        glClearNamedFramebufferfv(get_fbo(), GL_COLOR, 0, &clear_col.r);
        glClearNamedFramebufferfv(get_fbo(), GL_DEPTH, 0, &clear_depth);
        gl::check_error("DoubleBuffer::clear");
    }

    /// Swap the front & back buffer
    inline void swap() {
        swapped = !swapped;
    }

    /// Get the color texture of the front buffer
    inline GLuint get_col() {
        return swapped ? *a.col[0] : *b.col[0];
    }

    /// Get the depth texture of the front buffer
    inline GLenum get_depth() {
        if (!a.has_depth) {
            std::cerr << "Attempted to get depth texture of a double buffer without depth.\n";
            exit(1);
        }
        return swapped ? *a.depth : *b.depth;
    }
};
