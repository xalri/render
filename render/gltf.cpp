
#include "gltf.hpp"

Scene::Scene() {
    storage.textures.set_params(gl::SamplerParams::nice());
}

Material convert_material(tinygltf::Material const& m, GLint texture_offset) {
    auto material = Material { };

    auto _col_factor = m.values.find("baseColorFactor");
    if (_col_factor != m.values.end()) {
        auto col = _col_factor->second.ColorFactor();
        material.color_factor = v4 { (float) col.at(0), (float) col.at(1), (float) col.at(2), (float) col.at(3) };
    }

    auto _metal = m.values.find("metallicFactor");
    if (_metal != m.values.end()) material.metallic_factor = (float) _metal->second.Factor();

    auto _rough = m.values.find("roughnessFactor");
    if (_rough != m.values.end()) material.roughness_factor = (float) _rough->second.Factor();

    auto _emissive_factor = m.additionalValues.find("emissiveFactor");
    if (_emissive_factor != m.additionalValues.end()) {
        auto col = _emissive_factor->second.ColorFactor();
        material.emissive_factor = v3 { (float) col.at(0), (float) col.at(1), (float) col.at(2) };
    }

    auto _col = m.values.find("baseColorTexture");
    if (_col != m.values.end()) material.color_tx = _col->second.TextureIndex() + texture_offset;

    auto _mr = m.values.find("metallicRoughnessTexture");
    if (_mr != m.values.end()) material.mr_tex = _mr->second.TextureIndex() + texture_offset;

    auto _norm = m.additionalValues.find("normalTexture");
    if (_norm != m.additionalValues.end()) material.normal_tex = _norm->second.TextureIndex() + texture_offset;

    auto _ao = m.additionalValues.find("occlusionTexture");
    if (_ao != m.additionalValues.end()) material.ao_tex = _ao->second.TextureIndex() + texture_offset;

    auto _emissive = m.additionalValues.find("emissiveTexture");
    if (_emissive != m.additionalValues.end()) material.emissive_tex = _emissive->second.TextureIndex() + texture_offset;

    return material;
}

Scene::Mesh::Primitive convert_primitive(
        tinygltf::Primitive const& p,
        std::vector<tinygltf::Accessor> const& accessors,
        std::vector<tinygltf::BufferView> const& buffer_views,
        std::vector<tinygltf::Buffer> const& buffers,
        GLint material_offset
) {
    auto idx_vec = std::vector<IdxType>{};
    auto vert_vec = std::vector<Vertex>{};

    auto _get_buf = [&](size_t idx, GLenum type) -> uint8_t const* {
        auto accessor = accessors.at(idx);
        if (accessor.componentType != type) {
            std::cerr << "unexpected accessor type\n";
            exit(1);
        }
        auto view = buffer_views[accessor.bufferView];
        return buffers[view.buffer].data.data() + view.byteOffset;
    };

    auto get_buf = [&](std::string const &name, GLenum type) -> uint8_t const* {
        if (p.attributes.find(name) == p.attributes.end()) return nullptr;
        return _get_buf((size_t) p.attributes.at(name), type);
    };

    // TODO: convert from whatever the buffer contains to IdxType.
    auto idx_begin = (IdxType const*) _get_buf((size_t) p.indices, gl::type<IdxType>());
    auto vert_begin = (v3 const*) get_buf("POSITION", gl::type<GLfloat>());
    auto norm_begin = (v3 const*) get_buf("NORMAL", gl::type<GLfloat>());
    auto uv_begin = (v2 const*) get_buf("TEXCOORD_0", gl::type<GLfloat>());

    auto count = accessors[p.attributes.at("POSITION")].count;
    auto idx_count = accessors[p.indices].count;

    for (auto i = 0u; i < idx_count; i++) idx_vec.push_back(*(idx_begin + i));

    for (auto i = 0u; i < count; i++) {
        auto pos = v3(0.0);
        auto norm = v3(0.0);
        auto uv = v2(0.0);
        if (vert_begin) pos = *(vert_begin + i);
        if (norm_begin) norm = *(norm_begin + i);
        if (uv_begin) uv = *(uv_begin + i);
        vert_vec.push_back(Vertex{pos, norm, uv});
    }

    auto tri_vec = std::vector<Triangle>();
    for (auto i = 0; i < idx_count; i += 3) {
        tri_vec.push_back(Triangle {
                vert_vec[idx_vec[i]].pos,
                vert_vec[idx_vec[i + 1]].pos,
                vert_vec[idx_vec[i + 2]].pos,
        });
    }

    auto material = gl::Elem<Material> { (uint32_t)(std::max(p.material, 0) + material_offset) };

    return Scene::Mesh::Primitive { material, tri_vec, vert_vec, idx_vec };
}

std::optional<Anim::Channel> convert_channel(
        tinygltf::AnimationChannel const& c,
        tinygltf::Animation const& a,
        std::vector<tinygltf::Accessor> const& accessors,
        std::vector<tinygltf::BufferView> const& buffer_views,
        std::vector<tinygltf::Buffer> const& buffers,
        size_t node_offset
) {
    auto channel = Anim::Channel{};
    auto& sampler = a.samplers[c.sampler];

    auto key_accessor = accessors.at(sampler.input);
    auto key_view = buffer_views[key_accessor.bufferView];
    auto key_iter = buffers[key_view.buffer].data.data() + key_view.byteOffset;
    auto n_keys = key_accessor.count;

    auto value_accessor = accessors.at(sampler.output);
    auto value_view = buffer_views[value_accessor.bufferView];
    auto value_iter = buffers[value_view.buffer].data.data() + value_view.byteOffset;
    auto n_values = value_accessor.count;

    assert(n_keys == n_values);

    Anim::Target::Property property;
    auto elems = 0;
    if (c.target_path == "translation") { elems = 3; property = Anim::Target::Property::Translation; }
    if (c.target_path == "scale") { elems = 3; property = Anim::Target::Property::Scale;}
    if (c.target_path == "rotation") { elems = 4; property = Anim::Target::Property::Rotation;}

    if (elems == 0) {
        std::cerr << "Skipping unknown transform target '" << c.target_path << "'\n";
        return {};
    } else {
        auto node_idx = (size_t) (c.target_node + node_offset);
        channel.target = Anim::Target { property, node_idx };

        auto n = 0u;
        while (n < n_values) {
            v4 value = (elems == 3) ? v4(*((v3*) value_iter), 0.0) : *((v4*) value_iter);
            value_iter += elems * sizeof(float);
            channel.frames.push_back({ *((float*) key_iter), value });
            channel.length = channel.frames.back().time;
            key_iter += sizeof(float);
            n++;
        }

        return { channel };
    }
}

Node convert_node(tinygltf::Node const& node) {
    auto n = Node{};
    if (node.scale.size() >= 3) {
        n.scale = v3((float)node.scale[0], (float)node.scale[1], (float)node.scale[2]);
    }
    if (node.rotation.size() >= 4) {
        n.rotation = r3 { (float)node.rotation[3], (float)node.rotation[0], (float)node.rotation[1], (float)node.rotation[2] };
        assert(n.rotation.is_valid());
    }
    if (node.translation.size() >= 3) {
        n.translation = v3((float)node.translation[0], (float)node.translation[1], (float)node.translation[2]);
    }
    return n;
}

void Scene::import_gltf(std::string const &file_path, bool instantiate) {

    // ================= Load GLTF file ======================

    std::cerr << "Loading GLTF file\n";

    tinygltf::TinyGLTF loader{};
    tinygltf::Model model{};
    std::string err;
    std::string warn;

    auto ret = loader.LoadASCIIFromFile(&model, &err, &warn, file_path);

    if (!warn.empty()) printf("gltf warning: %s\n", warn.c_str());
    if (!err.empty()) printf("gltf error: %s\n", err.c_str());
    if (!ret) {
        printf("glTF parsing failed");
        exit(1);
    }


    // ================= Load Textures ======================

    std::cerr << "Loading textures\n";
    auto first_texture = storage.tx_size.size();

    for (auto n = 0u; n < model.images.size(); n++) {
        auto &i = model.images[n];
        std::cerr << "Inserting texture " << n << "\n";
        storage.insert_texture(i.width, i.height, i.image.data());
    }
    glGenerateTextureMipmap(storage.textures);
    gl::check_error("generate mipmaps");


    // ================= Load Materials ======================

    std::cerr << "Loading materials\n";
    auto first_material = storage.material.size();

    for (auto &m : model.materials) {
        auto material = convert_material(m, first_texture);
        storage.material.elem(material);
    }


    // ================= Load Meshes ======================

    std::cerr << "Loading meshes\n";

    for (auto& m : model.meshes) {
        auto primitives = std::vector<Mesh::Primitive>{};

        for (auto& p : m.primitives) {
            auto prim = convert_primitive(p, model.accessors, model.bufferViews, model.buffers, first_material);

            auto m_vert = storage.vert.view(prim.verts);
            auto m_idx = storage.idx.view(prim.indices);
            prim.mesh_data = { MeshData<Vertex> { v3(0, 0, 0), 0, m_vert, m_idx } };
            prim.mesh_data_idx = { storage.mesh.elem(*prim.mesh_data) };

            primitives.push_back(prim);
        }

        meshes.push_back(Mesh { primitives });
    }


    // ================= Load Animations ======================

    auto first_node = scene_graph.nodes.size();

    auto is_animated = std::vector<bool> {};
    is_animated.resize(model.nodes.size(), false);

    for (auto& a : model.animations) {
        auto anim = Anim {};
        for (auto& c : a.channels) {
            auto channel = convert_channel(c, a, model.accessors, model.bufferViews, model.buffers, first_node);
            if (channel.has_value()) {
                is_animated[channel->target.node - first_node] = true;
                anim.channels.push_back(*channel);
            }
        }
        animations.push_back(anim);
    }


    // ================== Instantiate objects ====================

    if (!instantiate) return;
    std::cerr << "Instantiating objects\n";

    // Parse the scene graph
    for (auto& node : model.nodes) {
        auto n = convert_node(node);
        scene_graph.nodes.push_back(n);
        transforms.elem(m4(1.0));
    }

    // Find node's parents
    for (auto i = 0u; i < model.nodes.size(); i++) {
        for (int j : model.nodes[i + first_node].children) {
            scene_graph.nodes[j + first_node].parent = i;
        }
    }

    // Calculate node depths
    scene_graph.fix_levels();

    // Instantiate objects
    for (auto i = 0u; i < model.nodes.size(); i++) {
        auto& node = model.nodes[i];
        if (node.mesh >= 0) {
            auto obj = Object { i + first_node, (size_t) node.mesh, node.name };

            if (node.extras.Has("collidable")) obj.collidable = node.extras.Get("collidable").Get<int>() == 1;
            if (node.extras.Has("visible")) obj.visible = node.extras.Get("visible").Get<int>() == 1;
            if (node.extras.Has("elasticity")) obj.elasticity = (float)node.extras.Get("elasticity").Get<double>();
            if (node.extras.Has("friction")) obj.friction = (float)node.extras.Get("friction").Get<double>();

            auto n = (int)(i + first_node);
            while (n != -1) {
                if (is_animated[n - first_node]) {
                    obj.dynamic = true;
                    break;
                }
                n = scene_graph.nodes[i].parent;
            }

            objs.push_back(obj);
        }
    }
}

void Scene::update_anims(float dt) {
    for(auto& a : animations) {
        a.update(scene_graph, dt);
    }
}
