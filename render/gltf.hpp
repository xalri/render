#pragma once

#include <vector>
#include <tiny_gltf.h>
#include "xal_scene_graph.h"
#include "common.hpp"
#include "vertex_format.hpp"
#include "draw.hpp"
#include "texture.hpp"
#include "camera.hpp"
#include "light.hpp"

#include "xal_collide.h" // TODO: temporary

static const GLsizei n_image_tx = 128;
static const GLsizei max_texture_size = 1024;

struct Vertex {
    v3 pos;
    v3 norm;
    v2 uv;
};

template<> inline gl::VertexFormat gl::vertex_format<Vertex>() {
    return gl::VertexFormat::make<3>({ gl::attr<v3>(), gl::attr<v3>(), gl::attr<v2>() });
}

struct alignas(sizeof(v4)) Material {
    v4 color_factor = v4(1.0f);
    v3 emissive_factor = v3(1.0f);
    float metallic_factor = 1.0f;
    float roughness_factor = 1.0f;
    GLint color_tx = -1;
    GLint mr_tex = -1;
    GLint ao_tex = -1;
    GLint normal_tex = -1;
    GLint emissive_tex = -1;
};

struct Lights {
    gl::Alloc<TubeLight> tube_lights{ 64 };
    gl::Alloc<SunLight> sun_lights{ 16 };
    gl::Alloc<SpotLight> spot_lights{ 64 };
    gl::Alloc<LightProbe> light_probes{ 16 };
};

struct Storage {
    gl::Alloc<Vertex> vert{1 << 20};
    gl::Alloc<IdxType> idx{1 << 22};
    gl::Alloc<MeshData<Vertex>> mesh{1 << 12};
    gl::Alloc<Material> material{1 << 12};
    gl::Alloc<v2> tx_size{(size_t) n_image_tx};
    gl::Texture2dArray textures { 9, GL_RGBA8, max_texture_size, max_texture_size, n_image_tx };

    void insert_texture(GLsizei width, GLsizei height, uint8_t const* data) {
        if (width > max_texture_size || height > max_texture_size) {
            std::cerr << "max texture size exceeded\n";
            exit(1);
        }
        glTextureSubImage3D(textures, 0, 0, 0, tx_size.head, width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);
        gl::check_error("Storage::insert_texture");
        tx_size.elem(v2((float) width, (float) height) /
                             v2((float) max_texture_size, (float) max_texture_size));
    }
};

/// A key-frame animation on some collection of scene graph nodes.
struct Anim {
    /// A single frame of animation
    struct Frame {
        float time = 0.0;
        v4 value{}; // for translation & scale last value is 0.
    };

    /// Gives the target node and property of a channel
    struct Target {
        enum class Property { Translation, Rotation, Scale } property;
        size_t node{};
    };

    /// Holds the key-frames and current state for one channel of animation
    struct Channel {
        Target target{};
        std::vector<Frame> frames{};
        size_t prev_frame = 0;
        float length = 0.0;

        /// Get the value of the channel at some time.
        inline std::optional<v4> value(float frame) {
            auto use_slerp = target.property == Target::Property::Rotation;
            auto target_time = std::fmod(frame, length);

            auto frame_idx = prev_frame;
            auto prev_frame_idx = prev_frame;
            auto frame_time = frames[prev_frame].time;
            auto prev_frame_time = frames[prev_frame].time;

            auto last_frame = frame_idx == 0 ? frames.size() - 1 : frame_idx - 1;

            while (frame_idx != last_frame) {
                frame_idx = (frame_idx + 1) % frames.size();
                frame_time = frames[frame_idx].time;

                if (prev_frame_time <= target_time && frame_time >= target_time) {
                    auto k = (target_time - prev_frame_time) / (frame_time - prev_frame_time);
                    auto v1 = frames[prev_frame_idx].value;
                    auto v2 = frames[frame_idx].value;
                    prev_frame = prev_frame_idx;

                    if (use_slerp) {
                        auto q1 = r3 { v1.w, v1.x, v1.y, v1.z };
                        auto q2 = r3 { v2.w, v2.x, v2.y, v2.z };
                        auto q = slerp(q1, q2, k); // slerp ensures interpolation will take the shorter path
                        return { v4 { q.w, q.x, q.y, q.z } };
                    } else {
                        return { mix(v1, v2, k) };
                    }
                } else {
                    prev_frame_time = frame_time;
                    prev_frame_idx = frame_idx;
                }
            }
            std::cerr << "Failed to find animation frame (this is a bug)\n";
            return {};
        }
    };

    std::vector<Channel> channels;
    bool active = true;
    float time = 0.0f;

    /// Update the animation.
    void update(SceneGraph& sg, float dt) {
        if (!active) return;
        time += dt;
        apply(sg, time);
    }

    /// Update scene nodes to reflect the animation at the given time
    void apply(SceneGraph& sg, float time) {
        for (auto& c : channels) {
            auto &node = sg.nodes[c.target.node];
            auto _v = c.value(time);
            if (!_v.has_value()) continue;
            auto v = *_v;

            if (c.target.property == Anim::Target::Property::Translation) node.translation = v3(v);
            if (c.target.property == Anim::Target::Property::Scale) node.scale = v3(v);
            if (c.target.property == Anim::Target::Property::Rotation) node.rotation = r3 { v.x, v.y, v.z, v.w };
        }
    }
};

struct Scene {
    struct Mesh {
        struct Primitive {
            gl::Elem<Material> material {};
            std::vector<Triangle> collision_tris {};
            std::vector<Vertex> verts {};
            std::vector<IdxType> indices {};
            std::optional<MeshData<Vertex>> mesh_data {};
            std::optional<gl::Elem<MeshData<Vertex>>> mesh_data_idx {};
        };

        /// The primitives which make up the mesh
        std::vector<Primitive> primitives;
    };

    struct Object {
        size_t scene_node;
        size_t mesh;
        std::string name = "";
        bool visible = true;
        // By default, true iff the object's node or any of it's parents is animated
        bool dynamic = false;
        bool collidable = false;
        float friction = 0.5;
        float elasticity = 0.5;
    };

    Storage storage {};
    Lights lights {};
    SceneGraph scene_graph{};
    gl::Alloc<m4> transforms { 1 << 10, gl::AllocKind::Mapped };
    std::vector<Mesh> meshes {};
    std::vector<Anim> animations {};

    std::vector<Object> objs {};
    std::vector<Camera> cameras {};

    Scene();

    /// Import a scene from a GLTF file.
    void import_gltf(std::string const& file_path, bool instantiate = true);

    /// Update animations
    void update_anims(float dt);

    /// Update the scene graph
    inline void update_graph() {
        scene_graph.update();
        // Update GPU buffer
        transforms.set_range(0, scene_graph.nodes.size(), scene_graph.global_transforms.data());
    }

    /// Build a batch of render objects containing all visible objects in the scene.
    inline std::vector<RenderObject<Vertex, Material>> render_objects() const {
        auto _o = std::vector<RenderObject<Vertex, Material>>{};
        for (auto& o : objs) {
            for (auto const& p : meshes.at(o.mesh).primitives) {
                _o.push_back(RenderObject<Vertex, Material>{
                        *p.mesh_data_idx,
                        gl::Elem<m4>{(uint32_t) (o.scene_node)},
                        p.material
                });
            }
        }
        return _o;
    }
};
