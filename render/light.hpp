#pragma once

#include "common.hpp"
#include "xal_math.h"

struct alignas(sizeof(v4)) SpotLight {
    v3 position;
    GLfloat range;
    v3 colour;
    GLfloat inner_cone;
    v3 forward;
    GLfloat outer_cone;
    GLboolean has_shadow = GL_FALSE;
    GLint shadow_map_idx = -1;
    m4 shadow_pv = m4(1.0);
    GLfloat pad[2] = {0};
};

struct alignas(sizeof(v4)) TubeLight {
    v3 position_a;
    GLfloat range;
    v3 position_b;
    GLfloat radius;
    v3 colour;
};

struct alignas(sizeof(v4)) SunLight {
    v3 direction;
    GLboolean has_shadow = GL_FALSE;
    v3 colour;
    GLint shadow_map_idx = -1;
    m4 shadow_pv = m4(1.0);
};

struct alignas(sizeof(GLfloat)) LightProbe {
    GLint map_idx;
    GLfloat factor;
};
