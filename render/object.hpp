#pragma once

#include "common.hpp"

namespace gl {
struct Object {
    GLuint id = 0;

    Object() = default;
    Object(Object&& rhs): id(rhs.id) { rhs.id = 0; }

    Object& operator=(Object&& rhs) {
        std::swap(id, rhs.id);
        return *this;
    }

    operator GLuint() const { return id; }

    Object(Object const&) = delete;
    Object& operator=(Object const&) = delete;
};
}