#include "shader.hpp"
#include "xal_util.h"
#include <sstream>
#include <fstream>

namespace gl {

Shader::Shader(GLenum kind, std::string const &source) {
    checked(id = glCreateShader(kind));
    auto str = source.c_str();
    checked(glShaderSource(id, 1, &str, nullptr));

    checked(glCompileShader(id));

    GLint status, log_length;
    checked(glGetShaderiv(id, GL_COMPILE_STATUS, &status));
    checked(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &log_length));

    if (log_length > 1) {
        auto info = std::make_unique<GLchar[]>((size_t) log_length + 1);
        glGetShaderInfoLog(id, log_length, nullptr, info.get());
        std::cerr << "Shader compilation log: " << info.get() << std::endl;
    }

    if (!status) {
        std::cerr << "Failed to compile shader\n";
        exit(1);
    }
}

std::stringstream shader_source(const char* filename) {
    std::clog << "Loading shader " << filename << "\n";
    std::stringstream ss{};
    std::string line;
    std::ifstream f(filename);
    if (!f.is_open()) {
        std::cerr << "Failed to open shader file '" << filename << "'\n";
        exit(1);
    }

    while(std::getline(f, line)) {
        const std::string p = "#pragma include ";
        if (line.compare(0, p.length(), p) == 0) {
            auto path = line.substr(p.length());
            trim(path);
            ss << shader_source(path.c_str()).str();
        } else {
            ss << line << "\n";
        }
    }

    return ss;
}

Shader Shader::from_file(GLenum kind, const char *filename) {
    auto ss = shader_source(filename);
    return Shader(kind, ss.str());
}

Shader::~Shader() {
    checked(glDeleteShader(id));
}

Program::Program(std::initializer_list<Shader> il) {
    checked(id = glCreateProgram());
    for (auto &shader : il) checked(glAttachShader(id, shader.id));

    checked(glLinkProgram(id));

    GLint status, log_length;
    checked(glGetProgramiv(id, GL_LINK_STATUS, &status));
    checked(glGetProgramiv(id, GL_INFO_LOG_LENGTH, &log_length));

    if (log_length > 1) {
        auto info = std::make_unique<GLchar[]>((size_t) log_length + 1);
        glGetProgramInfoLog(id, log_length, nullptr, info.get());
        std::cerr << "Program link log: " << info.get() << std::endl;
    }

    if (!status) {
        std::cerr << "Failed to link program\n";
        exit(1);
    }

    for (auto &shader : il) checked(glDetachShader(id, shader.id));

    GLint n_uniforms, n_ssbs;
    checked(glGetProgramInterfaceiv(id, GL_UNIFORM, GL_ACTIVE_RESOURCES, &n_uniforms));
    checked(glGetProgramInterfaceiv(id, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, &n_ssbs));

    for (auto unif = 0u; unif < n_uniforms; unif++) {
        GLint values[4];
        const GLenum properties[4] = { GL_BLOCK_INDEX, GL_TYPE, GL_NAME_LENGTH, GL_LOCATION };
        checked(glGetProgramResourceiv(id, GL_UNIFORM, unif, 4, properties, 4, nullptr, values));
        if (values[0] != -1) {
            std::cerr << "uniform blocks are unimplemented\n";
            exit(1);
        }
        std::string name((size_t) values[2], ' ');
        glGetProgramResourceName(id, GL_UNIFORM, unif, values[2], nullptr, &name[0]);
        name.pop_back(); // returned strings are null-terminated.
        uniforms.push_back({ name, values[3], values[1], {} });
    }

    for (auto ssb = 0u; ssb < n_ssbs; ssb++) {
        GLint values[2];
        const GLenum properties[2] = { GL_BUFFER_BINDING, GL_NAME_LENGTH };
        checked(glGetProgramResourceiv(id, GL_SHADER_STORAGE_BLOCK, ssb, 2, properties, 2, nullptr, values));
        std::string name((size_t) values[1], ' ');
        checked(glGetProgramResourceName(id, GL_SHADER_STORAGE_BLOCK, ssb, values[1], nullptr, &name[0]));
        name.pop_back(); // returned strings are null-terminated.

        ssbs.push_back({ name, values[0] });
    }
}

Program::~Program() {
    checked(glDeleteProgram(id));
}

void Program::use() const {
    checked(glUseProgram(id));
}

void Program::set_uniforms(Uniforms const &us) const {
    for (auto &u : uniforms) u.is_set = false;
    for (auto &s : ssbs) s.is_set = false;

    auto tx_unit = 0;
    for(auto& pair : us) {
        auto& name = pair.first;
        auto& value = pair.second;
        for (auto& s : ssbs) {
            if (s.name == name) {
                if (!value.is_buffer) {
                    std::cerr << "Invalid uniform, non-buffer object assigned to SSBO\n";
                    exit(1);
                }

                value.v_buffer->bind(GL_SHADER_STORAGE_BUFFER, s.binding);
                s.is_set = true;
            }
        }

        for (auto& u : uniforms) {
            if (u.name == name) {
                if (u.type != value.type) {
                    std::cerr << "Uniform type mismatch!\n";
                    exit(1);
                }
                switch (u.type) {
                    case GL_FLOAT: glProgramUniform1f(id, u.location, value.v_float); break;
                    case GL_INT: glProgramUniform1i(id, u.location, value.v_int); break;
                    case GL_UNSIGNED_INT: glProgramUniform1ui(id, u.location, value.v_uint); break;
                    case GL_FLOAT_VEC2: glProgramUniform2fv(id, u.location, 1, &value.v_vec2.x); break;
                    case GL_FLOAT_VEC3: glProgramUniform3fv(id, u.location, 1, &value.v_vec3.x); break;
                    case GL_FLOAT_VEC4: glProgramUniform4fv(id, u.location, 1, &value.v_vec4.x); break;
                    case GL_FLOAT_MAT4: glProgramUniformMatrix4fv(id, u.location, 1, GL_FALSE, &value.v_mat4.m00); break;
                    case GL_SAMPLER_2D_ARRAY: {
                        glBindTextureUnit((GLuint) tx_unit, *value.v_sampler2dArray);
                        glProgramUniform1i(id, u.location, tx_unit);
                        tx_unit += 1;
                        break;
                    }
                    case GL_SAMPLER_CUBE_MAP_ARRAY: {
                        glBindTextureUnit((GLuint) tx_unit, *value.v_samplerCubeArray);
                        glProgramUniform1i(id, u.location, tx_unit);
                        tx_unit += 1;
                        break;
                    }
                    default:
                        std::cerr << "Unimplemented uniform type!\n";
                        exit(1);
                }
                u.is_set = true;
                gl::check_error("Program::set_uniform");
                break;
            }
        }
    }

    for (auto &s : ssbs) {
        if (!s.is_set) {
            std::cerr << "Missing shader storage buffer '" << s.name << "'\n";
            exit(1);
        }
    }

    for (auto &u : uniforms) {
        if (!u.is_set) {
            std::cerr << "Missing uniform '" << u.name << "'\n";
            exit(1);
        }
    }
}
}
