#pragma once

#include "common.hpp"
#include "object.hpp"
#include "texture.hpp"
#include "buffer.hpp"
#include "xal_math.h"

namespace gl {

struct Uniform {
    GLenum type = 0;
    bool is_buffer = false;

    union {
        GLfloat v_float;
        GLint v_int;
        GLuint v_uint;
        v2 v_vec2;
        v3 v_vec3;
        v4 v_vec4;
        m4 v_mat4;
        Texture2dArray const* v_sampler2dArray;
        TextureCubeArray const* v_samplerCubeArray;
        Buffer<void> const* v_buffer;
        // ...
    };

    Uniform(): type(0) {}
    Uniform(GLfloat v): type(GL_FLOAT), v_float(v) {}
    Uniform(GLint v): type(GL_INT), v_int(v) {}
    Uniform(GLuint v): type(GL_UNSIGNED_INT), v_uint(v) {}
    Uniform(v2 v): type(GL_FLOAT_VEC2), v_vec2(v) {}
    Uniform(v3 v): type(GL_FLOAT_VEC3), v_vec3(v) {}
    Uniform(v4 v): type(GL_FLOAT_VEC4), v_vec4(v) {}
    Uniform(m4 v): type(GL_FLOAT_MAT4), v_mat4(v) {}
    Uniform(Texture2dArray const& v): type(GL_SAMPLER_2D_ARRAY), v_sampler2dArray(&v) {}
    Uniform(TextureCubeArray const& v): type(GL_SAMPLER_CUBE_MAP_ARRAY), v_samplerCubeArray(&v) {}
    template<class T>
    Uniform(Buffer<T> const& v): is_buffer(true), type(0), v_buffer(reinterpret_cast<Buffer<void> const*>(&v)) {}
    // ..
};

template <class T> inline Uniform uniform(T value);

struct UniformDefinition {
    std::string name;
    GLint location;
    GLint type;
    Uniform value;
    bool is_set = false;
};

struct SSB {
    std::string name;
    GLint binding;
    bool is_set = false;
};

using Uniforms = std::initializer_list<std::pair<const char*, Uniform>>;

/// A single shader
struct Shader: public Object {
    Shader(GLenum kind, std::string const &source);

    static Shader from_file(GLenum kind, const char* filename);

    ~Shader();
};

/// A shader program.
struct Program: public Object {
    mutable std::vector<UniformDefinition> uniforms;
    mutable std::vector<SSB> ssbs; // Shader storage blocks

    Program(std::initializer_list<Shader> il);

    static Program vert_frag(const char* vert_file, const char* frag_file) {
        return gl::Program({
            gl::Shader::from_file(GL_VERTEX_SHADER, vert_file),
            gl::Shader::from_file(GL_FRAGMENT_SHADER, frag_file)
        });
    }

    ~Program();

    void use() const;

    void set_uniforms(Uniforms const& us) const;
};

}
