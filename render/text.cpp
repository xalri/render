#include "text.hpp"

const char* TEXT_VERT_SOURCE = R"""(
    #version 450 core

    in vec2 pos;
    in float t;
    in float c;

    uniform mat4 view;
    uniform vec2 offset;

    out vec3 tpos;

    void main() {
        tpos = vec3(t * 0.5, max(t - 1.0, 0.0), c);
        gl_Position = view * vec4(pos + offset, 0.0, 1.0);
    }
)""";

const char* TEXT_FRAG_SOURCE = R"""(
    #version 450 core

    in vec3 tpos;

    out vec4 frag_col;

    void main() {
        float alpha = 1.0;
        if (tpos.z != 0.0) {
            vec2 p = tpos.xy;
            // Gradients
            vec2 px = dFdx(p);
            vec2 py = dFdy(p);
            // Chain rule
            float fx = ((2.0*p.x)*px.x - px.y);
            float fy = ((2.0*p.x)*py.x - py.y);
            // Signed distance
            float dist = fx*fx + fy*fy;
            float sd = (p.x*p.x - p.y)*-tpos.z/sqrt(dist);
            // Linear alpha
            alpha = clamp(0.5 - sd, 0.0, 1.0);
        }
        frag_col = alpha * vec4(1.0, 1.0, 1.0, 1.0);
    }

)""";
