#pragma once

#include "common.hpp"
#include "buffer.hpp"
#include "texture.hpp"
#include "context.hpp"
#include "vertex_format.hpp"
#include "ttf.hpp"
#include "alloc.hpp"
#include "xal_math.h"

template<> inline gl::VertexFormat gl::vertex_format<ttf::MeshVertex>() {
    return gl::VertexFormat::make<4>({ gl::attr<v2>(), gl::attr<int8_t>(), gl::attr<int8_t>(), gl::attr<int16_t>() });
}

extern const char *TEXT_VERT_SOURCE;
extern const char *TEXT_FRAG_SOURCE;

struct Font {
    constexpr static uint64_t first_char = 0x20;
    constexpr static uint64_t last_char = 0x7F;
    constexpr static uint64_t n_chars = last_char - first_char;

    std::array<std::pair<size_t, size_t>, n_chars> offset{};
    std::array<std::array<v2, n_chars>, n_chars> kerning{};
    gl::Buffer<ttf::MeshVertex> verts{};
    gl::Program program;

    explicit Font(std::string const& path):
        program { {gl::Shader(GL_VERTEX_SHADER, TEXT_VERT_SOURCE)}, {gl::Shader(GL_FRAGMENT_SHADER, TEXT_FRAG_SOURCE)} }
    {
        ttf::Font font(path);
        font.PreCacheBasicLatin();
        auto vert_data = std::vector<ttf::MeshVertex> {};

        for (auto i = first_char; i < last_char; i++) {
            auto& mesh = font.GetTriangulation(i).verts;
            offset[i - first_char] = { vert_data.size(), mesh.size() };
            for (auto v : mesh) vert_data.push_back(v);

            for (auto j = first_char; j < last_char; j++) {
                auto k = font.GetKerning(i, j);
                kerning[i - first_char][j - first_char] = v2{ k.x, k.y };
            }
        }

        verts = gl::Buffer<ttf::MeshVertex>(vert_data.size(), vert_data.data(), gl::BufferType::Static);
    }

    template<class Target>
    void draw_text(
        std::string const& str,
        gl::Context &ctx,
        Target const& target,
        gl::DrawParameters params,
        m4 transform,
        bool center = true
    ) {
        auto char_count = str.length();
        auto m = transform * scale(v3(0.0001));

        auto pos = v2 { 0.0, 0.0 };

        auto get_char = [&](size_t i) {
            auto c = (size_t) str[i];
            if (c < first_char || c > last_char) {
                std::cerr << "Skipping non-latin character '" << str[i] << "\n";
                c = '?';
            }
            return c;
        };

        if (center) {
            for (auto i = 0u; i < char_count; i++) {
                auto c = get_char(i) - first_char;
                if (i != char_count - 1) {
                    pos = pos - kerning[c][get_char(i+1) - first_char] * 0.5;
                } else {
                    pos = pos - kerning[c]['a' - first_char] * 0.5;
                }
            }
        }

        params.enable_culling = false;
        params.enable_blend = true;

        ctx.set_vertex_buffer(verts, {});
        for (auto i = 0u; i < char_count; i++) {
            auto c = get_char(i) - first_char;
            auto[first_vertex, count] = offset[c];

            ctx.pre_draw(target, program, params, {
                { "offset", pos },
                { "view", m },
            });
            glDrawArrays((GLenum) gl::PrimitiveType::Triangles, first_vertex, count);
            gl::check_error("Font::draw_text glDrawArrays");

            if (i != char_count - 1) {
                pos = pos + kerning[c][get_char(i+1) - first_char];
            }
        }
    }
};
