#include "texture.hpp"

namespace gl {

Texture2dArray Texture2dArray::from_file(const char *path) {
    int x, y, c;
    auto image_data = stbi_load(path, &x, &y, &c, STBI_rgb_alpha);
    if (!image_data) {
        std::cerr << "Failed to load '" << path <<  "': " << stbi_failure_reason() << "\n";
        exit(1);
    }
    auto tx = Texture2dArray(1, GL_RGBA8, x, y);
    glTextureSubImage3D(tx, 0, 0, 0, 0, x, y, 1, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    stbi_image_free(image_data);
    return tx;
}

Texture2dArray::Texture2dArray(GLsizei levels, GLenum internal_format, GLsizei width, GLsizei height, GLsizei depth) :
        Texture(GL_TEXTURE_2D_ARRAY) {
    checked(glTextureStorage3D(id, levels, internal_format, width, height, depth));
}

TextureCubeArray::TextureCubeArray(GLsizei levels, GLenum internal_format, GLsizei width, GLsizei height, GLsizei depth) :
        Texture(GL_TEXTURE_CUBE_MAP_ARRAY) {
    auto layers = depth * 6;
    checked(glTextureStorage3D(id, levels, internal_format, width, height, layers));
}
}