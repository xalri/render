#pragma once

#include <deps/tinygltf/stb_image.h>
#include "common.hpp"
#include "object.hpp"

namespace gl {

struct SamplerParams {
    enum Filter: GLenum {
        Nearest = GL_NEAREST,
        Linear = GL_LINEAR,
        NearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
        LinearMipmapNearest = GL_LINEAR_MIPMAP_NEAREST,
        NearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR,
        LinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR,
    };

    enum Wrap {
        Repeat = GL_REPEAT,
        Mirror = GL_MIRRORED_REPEAT,
        Clamp = GL_CLAMP_TO_EDGE,
        MirrorClamp = GL_MIRROR_CLAMP_TO_EDGE,
    };

    Filter magnify = Linear;
    Filter minify = Linear;
    Wrap wrap_x = Wrap::Repeat;
    Wrap wrap_y = Wrap::Repeat;
    Wrap wrap_z = Wrap::Repeat;
    uint16_t max_anisotropy = 1;

    static SamplerParams nice() {
        auto params = SamplerParams{};
        params.minify = Filter::LinearMipmapLinear;
        params.magnify = Filter::Linear;
        params.max_anisotropy = 8;
        return params;
    }
};

struct Texture: Object {
    SamplerParams params {};

    ~Texture() {
        if (id > 0) glDeleteTextures(1, &id);
        id = 0;
    }

    Texture(Texture&&) = default;
    Texture& operator=(Texture&&) = default;

    /// Update the texture to use the given sampling parameters
    void set_params(SamplerParams p) {
        if (p.magnify != params.magnify) glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, p.magnify);
        if (p.minify != params.minify) glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, p.minify);
        if (p.wrap_x != params.wrap_x) glTextureParameteri(id, GL_TEXTURE_WRAP_S, p.wrap_x);
        if (p.wrap_x != params.wrap_y) glTextureParameteri(id, GL_TEXTURE_WRAP_T, p.wrap_y);
        if (p.wrap_x != params.wrap_z) glTextureParameteri(id, GL_TEXTURE_WRAP_R, p.wrap_z);
        if (p.max_anisotropy != params.max_anisotropy) glTextureParameterf(id, GL_TEXTURE_MAX_ANISOTROPY, (GLfloat) p.max_anisotropy);
        params = p;
    }

protected:
    // Texture cannot be instantiated directly, must use Texture2D, Texture3D, ..
    explicit Texture(GLenum kind) {
        checked(glCreateTextures(kind, 1, &id));
    }
};

struct Texture2dArray: Texture {
    Texture2dArray(GLsizei levels, GLenum internal_format, GLsizei width, GLsizei height, GLsizei depth = 1);

    /// Load a single-layer texture from a file.
    static Texture2dArray from_file(const char* path);

    Texture2dArray(Texture2dArray&&) = default;
    Texture2dArray& operator=(Texture2dArray&&) = default;
};

struct TextureCubeArray: Texture {
    TextureCubeArray(GLsizei levels, GLenum internal_format, GLsizei width, GLsizei height, GLsizei depth = 1);
    TextureCubeArray(TextureCubeArray&&) = default;
    TextureCubeArray& operator=(TextureCubeArray&&) = default;
};

}