#pragma once

#include <cassert>
#include "common.hpp"

namespace gl {

/// The layout & types of some vertex attributes
struct VertexFormat {
    struct Attr {
        GLuint elements;
        GLuint size;
        GLenum kind;
        GLboolean normalized;
    };
    size_t n = 0;
    std::array<Attr, 20> attrs {};
    GLuint stride = 0;

    VertexFormat() = default;
    VertexFormat(VertexFormat &&) = default;

    template<size_t N>
    static VertexFormat make(std::array<Attr, N> attrs) {
        assert(N <= 20);
        VertexFormat fmt {};
        fmt.n = N;
        for (auto const& attr : attrs) fmt.stride += attr.size;
        std::copy(attrs.begin(), attrs.end(), fmt.attrs.begin());
        return fmt;
    }
};

/// Get the vertex format for some vertex attribute struct.
template<class T> inline VertexFormat vertex_format();

/// Build a VertexFormat::Attr for some primitive or glm type.
template<class T>
VertexFormat::Attr attr(GLboolean normalized = GL_FALSE) {
    return { (GLuint) n_elements<T>(), sizeof(T), type<T>(), normalized };
}

}