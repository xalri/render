#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <deque>
#include <array>
#include <optional>
#include "common.hpp"

enum class WindowEvKind {
    KeyPress,
    KeyRelease,
    MouseButtonPress,
    MouseButtonRelease,
    MouseMove,
    MouseWheelScroll,
    Resize,
    FramebufferResize,
    Closed,
    FocusGained,
    FocusLost,
    Moved
};

struct WindowEv {
    WindowEvKind kind;

    // Input
    struct Key{ int code, scancode, mods; };
    struct Mouse{ int button, mods; double x, y, x_diff, y_diff; };
    struct MouseWheel{ double x_off, y_off; };

    // Window
    struct WindowSize{ int width; int height; };
    struct FbSize{ int width; int height; };
    struct WindowPosition{ int x; int y; };

    union {
        Key key;
        Mouse mouse;
        MouseWheel mouse_wheel;
        WindowSize window_size;
        FbSize fb_size;
        WindowPosition window_position;
    };

    explicit WindowEv(WindowEvKind kind): kind(kind) {}
    WindowEv() {}
};

/// A wrapper around a GLFW window.
class Window /*: public gl::RenderTarget*/ {
public:
    explicit Window(const char *name = "window", int width = 1280, int height = 720);
    ~Window();

    Window(Window const&) = delete;
    Window& operator=(Window const&) = delete;
    Window& operator=(Window const&&) = delete;
    Window(Window&&);

    GLFWwindow* window = nullptr;
    std::deque<WindowEv> events{};
    std::array<bool, GLFW_MOUSE_BUTTON_LAST> mouse_pressed;
    std::array<bool, GLFW_KEY_LAST> key_pressed;
    v2 mouse{};
    v2 mouse_view{};
    v2 mouse_diff{};
    v2i size{};
    v2i _view_size{};
    v2i _view_offset{};
    double target_ratio;

    /// Poll for events
    void poll_events();

    /// Get the next event
    std::optional<WindowEv> next_event();

    /// Swap the framebuffers
    void display() const;

    /// Clear the window
    void clear(v4 clear_col, float clear_depth) {
        glClearNamedFramebufferfv(0, GL_COLOR, 0, &(clear_col.r));
        glClearNamedFramebufferfv(0, GL_DEPTH, 0, &clear_depth);
    }

    /// Check if the window is open
    bool should_close() const;

    /// Close the window
    void close();

    /// Set the number of buffer swaps to wait on when calling display.
    void set_vsync(int n);

    /// Bind the framebuffer for drawing.
    void bind_framebuffer() const {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        gl::check_error("Window::bind_framebuffer");
    }

    v2i view_size() const { return _view_size; }

    v2i view_offset() const { return _view_offset; }

    m4 ortho() const {
        return ::ortho(0, _view_size.x, _view_size.y, 0, -1.f, 1.f);
    }

    void set_draw_buffers() const {
        /*
        GLenum draw_buffers[] = { GL_BACK };
        glNamedFramebufferDrawBuffers(0, 1, draw_buffers);
        gl::check_error("Window::set_draw_buffers");
         */
    }
};
